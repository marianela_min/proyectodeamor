
<!DOCTYPE html>
<html lang="es">
<head>
<?php require_once ("css/style_config.php"); ?>
<title> PdA - Proyecto de Amor </title>
</head>

 
<body>
<?php include("inc/navigation.php") ?>
<main class="content-start">
    <div class="container">
        <div align="center" class="m-3 p-2">
        <h2>Una familia, un corazón</h2>
        <h4>somos Proyecto de Amor</h4>
        </div>
        <div class="row justify-content-around">
            <div class="col-md-4 text-center m-2">
            ¿Quiéres iniciar sesión?<br/><a type="button" class="btn btn-success btn-lg" href="login/login.php">Iniciar sesión</a>
            </div>
            <div class="col-md-4 text-center m-2">
                ¿No tienes una cuenta?<br/><a type="button" class="btn btn-primary btn-lg" href="login/signup.php">Regístrate aquí</a>
            </div>
            <div class="col-md-4 text-center m-2">
                Ir a la página principal: <a href="../index.htm">Proyecto de Amor</a>
            </div>
            <!-- <div align="center" class> </p></div> -->
        </div>
    </div>
    </main>
<?php include("inc/footer.php") ?>
<body>
</html>
