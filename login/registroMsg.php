<?php

// require_once "repeat.php";
// if ( !isset($_POST ) ) {
//     // Redirect the browser to login.php
//     redirect(0,urlencode("../index.php"));
//     return;
// }
?>

<!DOCTYPE html>
<html>
<head>
    <?php include("../css/style_config.php") ?>
    <title>Registro</title>
</head>
<body>
<?php include("../inc/navigation.php") ?>
<main class="content-start">
<div class="container">
<div class="row justify-content-center">
<div class="col-md-6 col-md-offset-3" align="center">
<h1>Proyecto de Amor</h1>
<h4><?= $title ?></h4>

<img class="img-fluid" width="20%" src="../images/logo.png" alt="PDA logo"><br><br>

<?= $msg ?>
        
</div></div></div>

</main>
<?php include("../inc/footer.php") ?>
</body>
</html>