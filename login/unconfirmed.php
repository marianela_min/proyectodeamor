<?php

require_once "../login/repeat.php";
require_once "../login/server.php";
require_once "../inc/db_mngmt.php";
require_once "../messages/messages.php";


if(!isset($_SESSION))
    session_start();

if(!isset($_SESSION['email']) || !isset($_SESSION['pass']) || !isset($_SESSION['userPrivilegio']))
    redirect(0,"login.php");

if($_SESSION['userPrivilegio'] !== 5)
    redirect(0,"session.php");


$email = $_SESSION['email'];
$loggedInTime = $_SESSION["loggedIn"];
$newMsg ='';

$whoPosted = 'u_s_id';

if(isset($_POST['u_s_id']))
{
    $whoPosted = 'u_s_id';
    $userToDel = array('i_id' => intval($_POST['id']),
        'i_em' => htmlentities($_POST['email']),
        'i_ci' => intval($_POST['ci']));
    $newMsg = borrar_unConfirmed($mysqli,$userToDel);
}

if(isset($_POST['m_s_id']))
{
    $whoPosted = 'm_s_id';
    $newMsg = eraseCiFrom($mysqli,intval($_POST['id']));
}

if(isset($_POST['m_s_addMoroso']))
{
    $whoPosted = 'm_s_id';

    // check that there are cedulas en textarea
    if(!empty($_POST['nuevosMorosos'])){
      // convert the string into an array of cedulas
      $str_ci_deudores = preg_split("/[ ]*,[ ]*/", htmlentities($_POST['nuevosMorosos']));
      // var_dump($str_ci_deudores);
      $ci_deudores = array();
      foreach($str_ci_deudores as $str_ci){
        $strToInt = preg_replace("/[^0-9]/","",$str_ci);
        if(strlen($strToInt) > 4){
          array_push($ci_deudores, intval($strToInt));
        }
      }
      // var_dump($ci_deudores);
      $newMsg = setMoroso($mysqli, $ci_deudores);

    }

}




$dataToUpdate = '';
if(isset($_POST['c_s_id']))
{
    $whoPosted = 'c_s_id';
    $userToMod = array('i_em' => htmlentities($_POST['email']),
        'i_ci' => htmlentities($_POST['ci']));
    $dataToUpdate = intent_to_update_ci_on_email($mysqli,$userToMod);//modif_cedulaErrada($mysqli,$userToMod);
    // echo '<pre>';
    // echo var_dump($dataToUpdate);
    // echo '</pre>';
}

if(isset($_POST['update_s_id'])){
    $whoPosted = 'c_s_id';
    // echo 'aqui se ejecuta la query ';
    $userToMod = array('i_id' => intval($_POST['id']),
                       'i_mis' =>  htmlentities($_POST['misionero']),
                       'i_ci' => htmlentities($_POST['ci']),
                       'i_em' => htmlentities($_POST['email']));
    $newMsg = update_ci($mysqli,$userToMod);
    $dataToUpdate = '';
}

$registrados = array();
if(isset($_POST['selecciona_s_id']))
{
  // se trae los registrados en el eventoID

    $whoPosted = 'r_s_id';
    if(isset($_POST['eventoID']))
    {
      $eventoID = intval(htmlentities($_POST['eventoID']));
      $registrados = get_inscritos_eventos($mysqli,$eventoID);
    }

}

if(isset($_POST['dr_s_id']))
{
  //desregistra al misionero
    $whoPosted = 'r_s_id';
    if(isset($_POST['userID']))
    {
        $newMsg = borrar_inscrito_evento($mysqli,intval(htmlentities($_POST['userID'])),intval(htmlentities($_POST['eventoID']))). '<p style="color:red;"> Si se trata de las misiones2020, debe eliminarlo manualmente del archivo de excel.</p>';
    }

}

if(isset($_POST['descargaReg_s_id']))
{
  //descarga la informacion de los que estan registrados
    $whoPosted = 'r_s_id';
    if(isset($_POST['eventoID']))
    {
      $eventoID = intval(htmlentities($_POST['eventoID']));

      $registrados = get_inscritos_eventos($mysqli,$eventoID);
      $fileheader = array();
      foreach ($registrados[0] as $key => $value) {
        array_push($fileheader, $key);
      }

      //var_dump($fileheader);

      // TODO: uncomment.
      $filename = '../descargas/'.'eventoPago_id_'.$eventoID.'.csv';
      // TODO: uncomment.
      $fp = fopen($filename, 'wb');
      // TODO: uncomment.
      fprintf($fp, chr(0xEF).chr(0xBB).chr(0xBF));

      // TODO: uncomment.
      fputcsv($fp, $fileheader);
      // echo '<!DOCTYPE><head> <meta charset="utf-8"></head><body>';
      // echo '<pre>';
      // var_dump($fileheader);
      // echo '</pre>';
      //
      // echo '<pre>';
      foreach($registrados as $fields)
           // TODO: uncomment.
           fputcsv($fp, $fields);
           // echo '<pre>';
           // var_dump($fields);
           // echo '</pre>';
      // TODO: uncomment.
      fclose($fp);
      // echo '</pre></body>';



      // TODO: uncomment.
      header("Location: ".$filename);

    }

}

if(isset($_POST['modifyEvent_s_id']))
{
  //TODO: modifica el evento
    $whoPosted = 'r_s_id';
    if(isset($_POST['eventoID']))
    {
      $eventoID = intval(htmlentities($_POST['eventoID']));

      //TODO: permitir a luisra modificar un evento.  Llevarlo s otra pagina para que haga la modificacion, O mostrarle un modal.
      echo 'Modificar evento en Construccion';
    }

}




$unConfirmedUsrs = get_unConfirmed($mysqli);

$morosos = get_ci($mysqli);

$allEventos = get_eventos($mysqli);










//if a valid user then I check for inactivity?
if(isset($_SESSION['usr_id']))
{
    if(time() - $loggedInTime > 1800) // time in seconds 1800 for 30min
    {
        header("Location: logout.php");
    }
    else{
        $_SESSION["loggedIn"] = time();
    }
}


?>

<!DOCTYPE html>
<html>
<head>
    <?php include("../css/style_config.php") ?>
    <title>Proyecto de amor</title>
    <style>
    .ss-cedula{
        border:1px solid #ffefea;
        opacity: 0.6;
    }
    .ss-cedula:hover{
        background: #fffaf2;
        color: #000000;
        opacity: 1;
    }
    .ss-cedula-number::after{
        /*content:'x';*/
    }

    .ss-icon{
        font-size: 2rem;
        color: #795548;
        text-align: center;
    }
    .ss-icon:hover{
        color: red;
    }

</style>
</head>
<body>

    <nav class="nav-text naveg-bar custom-nav">
        <ul class="nav justify-content-between">
            <li>Hola <em><?= $email ?></em>,</li>
            <li><a type="button" class="btn btn-danger btn-sm" href="logout.php">Cerrar Sesión <i class="fas fa-sign-out-alt"></i></a></li>
        </ul>
        <div class="small text-right">Su sesión expirará luego de <span id='loggedIn'>30 min</span> de inactividad.</div>
    </nav>

    <div align="center">
        <a type="button" class="btn btn-warning btn-sm" href=""><i class="fas fa-sync-alt"></i> Sesión</a>
        <a type="button" class="btn btn-outline-info btn-sm" href="session.php"> Volver</a>
    </div>

    <main class="content-start">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 col-md-offset-3" align="center">
                    <h1>Proyecto de Amor</h1>
                    <h4>Manejo de usuarios</h4>
                    <img class="img-fluid" width="20%" src="../images/logo.png" alt="PDA logo"><br/><br/>
                    <br/>
                </div></div></div>

                <div align="center"><span><?=$newMsg?></span></div>

                <div class="container">
                    <div class="row">
                        <ul class="nav nav-tabs col-auto">
                          <li class="nav-item">
                            <a class="nav-link <?= ($whoPosted == 'u_s_id')?' active': '' ?>" href="#unconfirmed" role="tab" data-toggle="tab">Sin Confirmar</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?= ($whoPosted == 'm_s_id')?' active': '' ?>" href="#morosos" role="tab" data-toggle="tab">Los Morosos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?= ($whoPosted == 'c_s_id')?' active': '' ?>" href="#cedulaErrada" role="tab" data-toggle="tab">Cédula Errada</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?= ($whoPosted == 'r_s_id')?' active': '' ?>" href="#registradosParaEvento" role="tab" data-toggle="tab">Registrados Para Evento</a>
                        </li>

                    </ul>


                    <div class="tab-content col-12">
                        <div role="tabpanel" class="tab-pane fade <?= ($whoPosted == 'u_s_id')?'show active': '' ?>" id="unconfirmed">
                        <p>Éstas son las cédulas de quienes por alguna razón no han podido confirmar su cuenta de correo, probablemente colocaron el correo mal al momento de registrarse.</p>
                         <ol class="row justify-content-around">
                             <?php

                             foreach($unConfirmedUsrs as $account)
                             {
                                echo '<li class="col-md-5 ss-cedula my-1">
                                <form action="" method="POST" class="row m-1 justify-content-between">

                                <div class="ss-cedula-number">
                                <input type="number" name="id" value="'.$account['usr_id'].'" hidden="">
                                <input type="number" name="ci" value="'.$account['ci'].'" hidden="">
                                <input type="text" name="email" value="'.$account['email'].'" hidden="">
                                <span class="small">Cédula:</span> V-'.$account['ci'].'<br/><span class="small">email:</span> '.$account['email'].' </div>
                                <div>
                                <button type="submit" name="u_s_id" class="ss-icon"><i class="fas fa-user-times"></i></button>
                                </div>
                                </form>
                                </li>';
                            }
                            ?>
                        </ol>
                        </div>

                        <div role="tabpanel" class="tab-pane fade <?= ($whoPosted == 'm_s_id')?' show active': '' ?>" id="morosos">
                            <p>Éstas son las cédulas de quienes no pudieran registrarse en el sistema debido a que presentan deuda(s) con el grupo.</p>
                            <div>
                              <form action="" method="POST">
                                <label>Coloque las cedulas separadas por coma. Ej: 12345678, 30876543</label><br/>
                                <textarea type="text" name="nuevosMorosos" cols="25" rows="4"></textarea>
                                <button type ="submit" name="m_s_addMoroso"> Agregar Moroso</button>
                              </form>
                            </div>
                            <hr/>
                            <ol class="row justify-content-around">
                             <?php
                             foreach($morosos as $ci)
                             {
    // echo '<div><span>'.$ci['ci_id'].' -- '.$ci['ci'].'</span></div>';
                                echo '<li class="col-md-7 ss-cedula my-2">
                                <form action="" method="POST" class="row m-1 justify-content-between">
                                <div class="ss-cedula-number">
                                <input type="number" name="id" value="'.$ci['ci_id'].'" hidden="">
                                <span class="small">Cédula:</span> V-'.$ci['ci'].' </span>
                                </div>
                                <div>
                                <button type="submit" name="m_s_id" class="ss-icon"><i class="fas fa-trash"></i> Borrar</button>
                                </div>
                                </form>
                                </li>';
                             }
                                ?>
                            </ol>
                        </div>

                        <div role="tabpanel" class="tab-pane fade <?= ($whoPosted == 'c_s_id')?' show active': '' ?>" id="cedulaErrada">
                             <p>Para modificar la cédula de un misionero cuyo correo es correcto y luego de llenar planilla se da cuenta que la cédula esta errada.  Es posible que tengamos casos inocentes como éste, sin embargo para evitar que hagan trampa y evadir el pago, se presentan dos campos: la cédula nueva y el correo correcto.<br/>El sistema chequea que la nueva cédula este disponible y solvente, de tener exito se habilitará un boton para que se guarden los cambios, de lo contrario se mostrará un mensaje indicando la razón del por qué no se puede hacer el cambio.</p>
                                <div class="row justify-content-around">
                                    <div  align="left" class="col-md-6 ss-cedula my-2">
                                    <form action="" method="POST" class="row justify-content-between">
                                        <div class="col-md-9 ss-cedula-number p-3">
                                             <span class="small">Cédula correcta:</span>
                                               V-<input class="form-control" id="ci" type="text" oninput="isvalid(this.id,'cedula')" name="ci" placeholder="coloca la cédula correcta..." required="true"/>
                                              <span class="small">Correo:</span>
                                               <input class="form-control" id="email" type="text" oninput="isvalid(this.id,'email')" name="email" placeholder="coloca el correo..." required="true"/>
                                               <div class="required-message" hidden>Corrija las entradas</div>

                                        </div>
                                        <button type="submit" name="c_s_id" class=" col-md-3 ss-icon"><i class="fas fa-search"></i></button>
                                    </form>
                                    </div>
                                   <?= reporte_para_cambiar_cedula($dataToUpdate) ?>
                                    </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade <?= ($whoPosted == 'r_s_id')?' show active': '' ?>" id="registradosParaEvento">
                             <p>Selecciona el Evento al cual deseas modificar sus asistentes.</p>
                                <div class="row justify-content-around">
                                    <div  align="left" class="col-12 my-2">
                                      <form action="" method="POST" class="row m-1 justify-content-center">
                                        <select name="eventoID" >
                                          <?php
                                             foreach ($allEventos as $evento): {
                                               // var_dump($evento);
                                               echo '<option value="'.$evento['eventoID'].'">'.$evento['nombreEvento'].'</option>';
                                             }
                                          ?>
                                        <?php endforeach; ?>
                                        </select>
                                        <button type="submit" name="selecciona_s_id" class=" col-md-2 ss-icon"><i class="fas fa-search"></i></button>
                                        <button type="submit" name="descargaReg_s_id" class=" col-md-2 ss-icon"><i class="fas fa-download"></i></button>
                                        <button type="submit" name="modifyEvent_s_id" class=" col-md-2 ss-icon"><i class="fas fa-edit"></i></button>
                                      </form>
                                    </div>
                                </div>
                                <div class="row justify-content-around">
                                    <div align="left" class="col-12">
                                      <ol>
                                        <?php
                                        foreach ($allEventos as $evento)
                                        {
                                          print_r($evento);
                                          if($evento['eventoID'] == $eventoID){
                                            echo '<p>$evento["nombreEvento"]</p>';
                                          }
                                        }

                                        foreach($registrados as $misionero)
                                        {
                                        // echo '<div><span>'.$ci['ci_id'].' -- '.$ci['ci'].'</span></div>';
                                           echo '<li class="col-md-5 ss-cedula my-2">
                                           <form action="" method="POST" class="row m-1 justify-content-between">
                                           <div class="ss-cedula-number">
                                           <input type="number" name="eventoID" value="'.$eventoID.'" hidden="">
                                           <input type="number" name="userID" value="'.$misionero['id'].'" hidden="">
                                           <span class="small">Nombre: </span>'.$misionero['NombreCompleto'].' </span><br/>
                                           <span class="small">Cédula: </span>V-'.$misionero['CedulaDelMisionero'].' </span>
                                           </div>
                                           <div>
                                           <button type="submit" name="dr_s_id" class=""><i class="fas fa-trash"></i> Anular Inscripcion</button>
                                           </div>
                                           </form>
                                           </li>';
                                        }
                                           ?>
                                      </ol>
                                    </div>

                                    </div>
                        </div>
                      </div>


</div>

</div>

</main>
<?php include("../inc/footer.php") ?>
</body>
</html>
