<?php


// redirect(0,"test_validation.txt?email=malsdk@ladf.com&rest=1");


function regexfor($s){
    switch ($s) {
        case 'ful-name':
        //matches that my name contains only A-Z a-z and up to 4 words with spaces in between
            $regEx = "/^[A-Za-zñáéíóúÑÁÉÍÓÚ\/]+\s?[A-Za-zñáéíóúÑÁÉÍÓÚ\/]*\s?[A-Za-zñáéíóúÑÁÉÍÓÚ\/]*\s?[A-Za-zñáéíóúÑÁÉÍÓÚ\/]*$/";
            break;
        case 'name':
            $regEx = "/^[A-Za-zñáéíóúÑÁÉÍÓÚ\/]+$/";
            break;
        case 'email':
            $regEx = "/^(:?[a-z][\w\.\-]*)@[a-z][\w\.\-]*?(=?\.[a-z]{2,4})$/";
            break;
        case 'pass-w':
            $regEx = "/^.{8,}$/";
            break;
        case 'pass':
            $regEx = "/^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?!.*[\s]).{7,}$/";
            break;
        case 'phone':
            $regEx = "/^\d{4}\-\d{7}$/";
            break;
        case 'dob':
            $regEx = "/^((:?19|20)\d{2})\-?([01]\d)\-?([0123]\d)$/"; // YYYY-MM-DD
            break;
        case 'cedula':
            $regEx = "/^\d{6,11}$/";
        case 'numero':
            $regEx = "/^\d{10,11}$/";
            break;
        case 'any':
            $regEx = "/^[ñáéíóúÑÁÉÍÓÚ\w\s\(\)\,\#\.\-\/\+]{1,200}$/"; //this is anything
            // echo "regex any en uso:  ".$regEx."\n";
            break;

        default:
            $regEx = "/^[ñáéíóúÑÁÉÍÓÚ\w\s\(\)\,\#\.\-\/]{0,90}$/";
            break;
    }

return $regEx;
}

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }

function redirect($p=-1,$s="index.php"){
    switch($p){
        case -1:
            header("Location: ../".$s);
            exit;
        case 0:
            header("Location: ".$s);
            exit;
            break;
    }
  }

function valid_ci($numero){

    $min = 1000000; // un millon
    $max = 50000000; // 50 millones
    if( $numero > $min && $numero < $max )
        return true;
    else
        return false;
}

function fechaRange($misionero, $cupo, $evento){
    $dt = new DateTime("now", new DateTimeZone('America/Caracas'));
    $dt_start_nuevos = new DateTime("2020-01-12T00:00:00.0", new DateTimeZone('America/Caracas'));
    // print_r($dt_start_nuevos);

    // echo $evento['openRegDate']->format('d-m-Y H:i:s');
    // echo '<br>';
    // echo $evento['closeRegDate']->format('d-m-Y H:i:s');
    // echo '<br> <pre>';
    // print_r($dt->diff($evento['openRegDate']));
    // echo '</pre>';
    // echo $dt->diff($evento['openRegDate'])->format('%m meses y %d días');
    // echo '<br> <pre>';
    // print_r(($evento['openRegDate']->diff($dt))->invert);
    // echo '</pre>';

    $bol = array();


    if(($evento['openRegDate']->diff($dt))->invert == 0 && ($dt->diff($evento['closeRegDate']))->invert == 0){
      // echo "Im in range";
      // echo "misionero: ". $misionero;
      if($misionero == 0){
        // echo "I am a new misionero";
        if(($dt_start_nuevos->diff($dt))->invert == 0){
          $aprobado = 1;
        }else{
          $aprobado = 0;
        }
        // echo ": " .$aprobado;
      }else{
        $aprobado = 1;
      }
    }else{
      // echo "not in range";
      $aprobado = 0;
    }

    // proceed after checking dates
    if($aprobado === 1){
      // proceed witht the registration
      if($evento['maxCupos']-intval($cupo) > 0){
        $bol['msg'] = '<p style="color:green">Tu solicitud de inscripción ha sido enviada</p>';
        $bol['ready'] = 1;
      }else{
        // decline the registration
        $bol['msg'] =  '<p style="color:#f44336">¡Lo sentimos! Tu inscripción no pudo ser enviada, los cupos se han agotado</p>';
      }
    }else{
      // decline the registration
      $bol['ready'] = 0;
      $bol['msg'] = '<p style="color:gray">La inscripción a ' . $evento['name'] . ' aún no está habilitada</p>';
    }




    // echo $dt->format('m/d/Y, H:i:s');
    // $mes = intval($dt->format('m'));
    // $dia_actual = intval($dt->format('d'));
    // $dia_actual = $_GET['actual'];
    // if($mes==1){
    //     if(750-intval($cupo)>0){
    //         if($misionero==1 && $dia_actual>=7 && $dia_actual<=18){
    //             $bol['msg'] = '<p style="color:green">Tu solicitud de inscripción ha sido enviada</p>';
    //             $bol['ready'] = 1;
    //         }
    //         else if($misionero==0 && $dia_actual>=14 && $dia_actual<=18){
    //             $bol['msg'] = '<p style="color:green">Tu solicitud de inscripción ha sido enviada</p>';
    //             $bol['ready'] = 1;
    //         }
    //     }
    //     else{
    //             $bol['msg'] =  '<p style="color:#f44336">¡Lo sentimos! Tu inscripción no pudo ser enviada, los cupos se han agotado</p>';
    //         }
    // }
    return $bol;
}
