<footer class="p-4 custom-footer">
	<div class="container-fluid">
		<div class="row align-items-center">
			<h4>¡Gracias por tu apoyo y participación!</h4>
		</div>
		<div class="row align-items-center">
			<div class="col-5 col-sm-3 col-md-2 col-lg-2">
				<div class="small text-center pb-2"><img class="img-fluid" width="20%" src="<?= $confBaseUrl?>/miembro/images/logo.png" alt="PDA logo"></div>
			</div>
			<span class="col col-sm col-md"></span>
			<div class="col-12 col-sm-5 col-md-4 col-lg-5 text-center py-2">
				<span class=" small text-center"> <a href="<?= $confBaseUrl?>" target="_blank">PDA.ORG.VE</a></span>
				<span class="small text-center"><address>{Dirección}<br />Barquisimeto, Lara<br />Venezuela</address></span>
				<span class="small text-center"><a href="mailto:proyectodeamor@gmail.com" style="opacity:0.6;">proyectodeamor@gmail.com</a></span>
			</div>
		</div>
	</div>
</footer>

<div class="container-fluid px-3 custom-footer">
	<div class="row align-items-strech">
		<div class="col-12 small text-center"><span>Diseñado y desarrollado por voluntarios de Dios</span></div>
		<div class="col-12 small text-center"><span>Derechos reservados 2019</span></div>
	</div>
</div>

<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<!-- <script src="http://pda.org.ve/miembro/js/scripts.js" ></script> -->
<script src="https://pda.org.ve/miembro/js/scripts.js" ></script>
