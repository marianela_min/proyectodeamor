<?php
if(!isset($_SESSION)) session_start();
require_once "repeat.php";
if ( empty($_POST) ) {
  // echo("Aca entro y quiere redireccionar a signup;");
  redirect(-1,"index.php");
  die;
}

if(!isset($_SESSION['email']) || !isset($_SESSION['pass']))
    redirect(0,"login.php");

function atento($word){
      $dt = new DateTime("now", new DateTimeZone('America/Caracas'));
  return '<div align="center" class="popup-message"><p style="color:green;">Formulario '.$word.' exitosamente</p>
                      <p style="color:#ff5722; font-weight:bold;">Atento(a) a las fechas publicadas para que te inscribas en el próximo evento. <br/><span class="small">(Hoy es: '.$dt->format('d/m/Y').')</span></p></div>';
}
// echo '<pre>';
// var_dump($_POST);
// echo '</pre>';
// echo isset($_POST['cancel']);
// die;
if(isset($_POST['cancel'])){
  $_SESSION['msg'] = '<div class="popup-message"><p style="color:gray;">No se guardó ningún cambio</p></div>';

  $_SESSION['error_entry_2055165912'] = '';
  $_SESSION['error_entry_498994277'] = '';
  $_SESSION['error_entry_359752058'] = '';
  $_SESSION['error_entry_119140095'] = '';
  // $_SESSION['error_entry_2005063964'] = '';
  $_SESSION['error_entry_1613747071'] = '';
  $_SESSION['error_entry_2128285595'] = '';
  $_SESSION['error_entry_2101364991'] = '';
  $_SESSION['error_RepNombre'] = '';
  $_SESSION['error_RepTel'] = '';
  $_SESSION['error_RepParentesco'] = '';


  $_SESSION['error_entry_1309052470'] = '';
  $_SESSION['error_entry_1677385248'] = '';
  // $_SESSION['error_entry_1522088102'] = '';
  $_SESSION['error_entry_514490750'] = '';

  $_SESSION['error_entry_1338231974'] = '';
  $_SESSION['error_entry_446411834'] = '';
  $_SESSION['error_entry_1241364390'] = '';
  $_SESSION['error_entry_80365004'] = '';
  $_SESSION['error_sacramento'] = '';

  $_SESSION['error_entry_107097543'] = '';
  $_SESSION['error_noCatolica'] = '';
  $_SESSION['error_entry_1204949772'] = '';
  $_SESSION['error_misiones'] = '';
  $_SESSION['error_entry_992502456'] = '';
  $_SESSION['error_habilidad'] = '';
  $_SESSION['error_entry_1252199433'] = '';

  $_SESSION['error_entry_507951984'] = '';
  $_SESSION['error_entry_1907784252'] = '';
  $_SESSION['error_entry_2069387399'] = '';
  $_SESSION['error_entry_1262205387'] = '';

  $_SESSION['error_entry_704382283'] = '';
  $_SESSION['error_entry_147384584'] = '';
  $_SESSION['error_entry_1941103996'] = '';
  $_SESSION['error_entry_1126146889'] = '';
  $_SESSION['error_entry_1560839713'] = '';
  $_SESSION['error_emigrar'] = '';

  redirect(0,"session.php");
  exit;
}
if(isset($_POST['submit'])){

// echo '<pre>This is the post content:';
// var_dump($_POST);
// echo'</pre>';

// echo '<pre style="color:blue;">This is the session content:';
// var_dump($_SESSION);
// echo'</pre>';

// echo '<div style="border-bottom: 1px solid red;" width = 100%> ------------- </div>';

$insertUpdate = $_SESSION['insertUpdate'];
$failure = 0;

$atleast_one_field_is_filled = !empty($_POST['error_entry_2055165912']) || !empty($_POST['error_entry_498994277']) || !empty($_POST['error_entry_359752058']) || !empty($_POST['error_entry_119140095']) || !empty($_POST['error_entry_1613747071']) || !empty($_POST['error_entry_2128285595']) || !empty($_POST['error_entry_2101364991']) || !empty($_POST['error_entry_1309052470']) || !empty($_POST['error_entry_1677385248']) || !empty($_POST['error_entry_514490750']) ||
 !empty($_POST['error_entry_1338231974']) || !empty($_POST['error_entry_446411834']) || !empty($_POST['error_entry_1241364390']) || !empty($_POST['error_entry_80365004']) || !empty($_POST['error_sacramento']) ||
 !empty($_POST['error_entry_107097543']) || !empty($_POST['error_entry_1204949772']) || !empty($_POST['error_misiones']) || !empty($_POST['error_entry_992502456']) || !empty($_POST['error_habilidad']) || !empty($_POST['error_entry_1252199433']) ||
 !empty($_POST['error_entry_507951984']) || !empty($_POST['error_entry_1907784252']) || !empty($_POST['error_entry_2069387399']) || !empty($_POST['error_entry_1262205387']) ||
 !empty($_POST['error_entry_704382283']) || !empty($_POST['error_entry_147384584']) || !empty($_POST['error_entry_1941103996']) || !empty($_POST['error_entry_1126146889']) || !empty($_POST['error_entry_1560839713']);



if($atleast_one_field_is_filled === false){

require ("server.php");

$usr_id  = $_SESSION['usr_id'];
$name1  = $_SESSION['entry_2055165912'] =   $mysqli->escape_string($_POST['entry_2055165912']);
$name2  = $_SESSION['entry_498994277'] =   $mysqli->escape_string($_POST['entry_498994277']);
$last1  = $_SESSION['entry_359752058'] =   $mysqli->escape_string($_POST['entry_359752058']);
$last2  = $_SESSION['entry_119140095'] =   $mysqli->escape_string($_POST['entry_119140095']);
// $ci  = $_SESSION['cedula'];
$talla  = $_SESSION['entry_1613747071'] =  $mysqli->escape_string($_POST['entry_1613747071']);
$sexo  = $_SESSION['entry_2128285595'] =  $mysqli->escape_string($_POST['entry_2128285595']);
$dob  = $_SESSION['entry_2101364991'] =  $mysqli->escape_string($_POST['entry_2101364991']);


$rep_nom = $_SESSION['RepNombre'] = $mysqli->escape_string($_POST['RepNombre']);
$rep_tel = $_SESSION['RepTel'] = $mysqli->escape_string($_POST['RepTel']);
$rep_parentesco = $_SESSION['RepParentesco'] = $mysqli->escape_string($_POST['RepParentesco']);


$tel_fijo  = $_SESSION['entry_1309052470'] =  $mysqli->escape_string($_POST['entry_1309052470']);
$tel_mobil  = $_SESSION['entry_1677385248'] =  $mysqli->escape_string($_POST['entry_1677385248']);
// $email  = $_SESSION['entry_1522088102'] =  $mysqli->escape_string($_POST['entry_1522088102']);
$direccion  = $_SESSION['entry_514490750'] =  $mysqli->escape_string($_POST['entry_514490750']);

$dond_estudia  = $_SESSION['entry_1338231974'] =  $mysqli->escape_string($_POST['entry_1338231974']);
$que_estudia  = $_SESSION['entry_446411834'] =  $mysqli->escape_string($_POST['entry_446411834']);
$dond_trabaja  = $_SESSION['entry_1241364390'] =  $mysqli->escape_string($_POST['entry_1241364390']);
$profesion  = $_SESSION['entry_80365004'] =  $mysqli->escape_string($_POST['entry_80365004']);
$sacramento  = $_SESSION['sacramento'] =  $mysqli->escape_string($_POST['sacramento']);

$o_catolica  = $_SESSION['entry_107097543'] =  $mysqli->escape_string($_POST['entry_107097543']);
$no_catolica = $_SESSION['noCatolica'] =  $mysqli->escape_string($_POST['noCatolica']);
$referido  = $_SESSION['entry_1204949772'] =  $mysqli->escape_string($_POST['entry_1204949772']);
$pre_misiones  = $_SESSION['misiones'] =  $mysqli->escape_string($_POST['misiones']);
$novio  = $_SESSION['entry_992502456'] =  $mysqli->escape_string($_POST['entry_992502456']);
$habilidades  = $_SESSION['habilidad'] =  $mysqli->escape_string($_POST['habilidad']);
$o_habilidades  = $_SESSION['entry_1252199433'] =  $mysqli->escape_string($_POST['entry_1252199433']);

$tipo_sangre  = $_SESSION['entry_507951984'] =  $mysqli->escape_string($_POST['entry_507951984']);
$enfermedad  = $_SESSION['entry_1907784252'] =  $mysqli->escape_string($_POST['entry_1907784252']);
$medicamento  = $_SESSION['entry_2069387399'] =  $mysqli->escape_string($_POST['entry_2069387399']);
$alergia  = $_SESSION['entry_1262205387'] =  $mysqli->escape_string($_POST['entry_1262205387']);

$papa_fulname  = $_SESSION['entry_704382283'] =  $mysqli->escape_string($_POST['entry_704382283']);
$papa_tel  = $_SESSION['entry_147384584'] =  $mysqli->escape_string($_POST['entry_147384584']);
$mama_fulname  = $_SESSION['entry_1941103996'] =  $mysqli->escape_string($_POST['entry_1941103996']);
$mama_tel  = $_SESSION['entry_1126146889'] =  $mysqli->escape_string($_POST['entry_1126146889']);
$disp_carro  = $_SESSION['entry_1560839713'] =  $mysqli->escape_string($_POST['entry_1560839713']);
$emigrar  = $_SESSION['emigrar'] =  $mysqli->escape_string($_POST['emigrar']);
// $newvariable = $mysqli->escape_string($_POST['draftResponse']);
// $newvariable = $mysqli->escape_string($_POST['pageHistory']);
// $newvariable = $mysqli->escape_string($_POST['fbzx']);
$submit_bttn  = $_SESSION['submit'] =   $mysqli->escape_string($_POST['submit']);

$_SESSION['error_entry_2055165912'] = '';
$_SESSION['error_entry_498994277'] = '';
$_SESSION['error_entry_359752058'] = '';
$_SESSION['error_entry_119140095'] = '';
// $_SESSION['error_entry_2005063964'] = '';
$_SESSION['error_entry_1613747071'] = '';
$_SESSION['error_entry_2128285595'] = '';
$_SESSION['error_entry_2101364991'] = '';
$_SESSION['error_RepNombre'] = '';
$_SESSION['error_RepTel'] = '';
$_SESSION['error_RepParentesco'] = '';


$_SESSION['error_entry_1309052470'] = '';
$_SESSION['error_entry_1677385248'] = '';
// $_SESSION['error_entry_1522088102'] = '';
$_SESSION['error_entry_514490750'] = '';

$_SESSION['error_entry_1338231974'] = '';
$_SESSION['error_entry_446411834'] = '';
$_SESSION['error_entry_1241364390'] = '';
$_SESSION['error_entry_80365004'] = '';
$_SESSION['error_sacramento'] = '';

$_SESSION['error_entry_107097543'] = '';
$_SESSION['error_noCatolica'] = '';
$_SESSION['error_entry_1204949772'] = '';
$_SESSION['error_misiones'] = '';
$_SESSION['error_entry_992502456'] = '';
$_SESSION['error_habilidad'] = '';
$_SESSION['error_entry_1252199433'] = '';

$_SESSION['error_entry_507951984'] = '';
$_SESSION['error_entry_1907784252'] = '';
$_SESSION['error_entry_2069387399'] = '';
$_SESSION['error_entry_1262205387'] = '';

$_SESSION['error_entry_704382283'] = '';
$_SESSION['error_entry_147384584'] = '';
$_SESSION['error_entry_1941103996'] = '';
$_SESSION['error_entry_1126146889'] = '';
$_SESSION['error_entry_1560839713'] = '';
$_SESSION['error_emigrar'] = '';



// validation starts here

// echo "<h1> Tengo $failure errores ANTES de validation </h1>";

  if(!preg_match(regexfor('name'),$name1)){
  $_SESSION['error_entry_2055165912'] = 'corrija este campo';
    $failure = $failure +1;
  }
  if(!preg_match(regexfor('name'),$name2)){
  $_SESSION['error_entry_498994277'] = 'corrija este campo';
    $failure = $failure +1;
  }
  if(!preg_match(regexfor('name'),$last1)){
  $_SESSION['error_entry_359752058'] = 'corrija este campo';
    $failure = $failure +1;
  }
  if(!preg_match(regexfor('name'),$last2)){
  $_SESSION['error_entry_119140095'] = 'corrija este campo';
    $failure = $failure +1;
  }
  // if(!valid_ci($ci)){
  //   $_SESSION['error_entry_2005063964'] = 'corrija este campo';
  //   $failure = $failure +1;
  // }

  if(strcmp($talla,"")==0){
  $_SESSION['error_entry_1613747071'] = 'corrija este campo';
    $failure = $failure +1;
  }
  if(!preg_match(regexfor('any'),$sexo)){
  $_SESSION['error_entry_2128285595'] = 'corrija este campo';
    $failure = $failure +1;
  }

  if(!preg_match(regexfor('dob'),$dob)){
  $_SESSION['error_entry_2101364991'] = 'corrija este campo';
    $failure = $failure +1;
  }

    if(!preg_match(regexfor('ful-name'),$rep_nom)){
    $_SESSION['error_RepNombre'] = 'corrija este campo';
      $failure = $failure +1;
  }

  if(!preg_match(regexfor('any'),$rep_tel)){
    $_SESSION['error_RepTel'] = 'corrija este campo';
      $failure = $failure +1;
  }

  if(!preg_match(regexfor('any'),$rep_parentesco)){
    $_SESSION['error_RepParentesco'] = 'corrija este campo';
      $failure = $failure +1;
  }

  if(!preg_match(regexfor('any'),$tel_fijo)){
  $_SESSION['error_entry_1309052470'] = 'corrija este campo';
    $failure = $failure +1;
  }
  if(!preg_match(regexfor('any'),$tel_mobil)){
  $_SESSION['error_entry_1677385248'] = 'corrija este campo';
    $failure = $failure +1;
  }
  // if(!preg_match(regexfor('email'),$email)){
  // $_SESSION['error_entry_1522088102'] = 'corrija este campo';
  //   $failure = $failure +1;
  // }
  if(!preg_match(regexfor('any'),$direccion)){
  $_SESSION['error_entry_514490750'] = 'corrija este campo';
    $failure = $failure +1;
  }

  if(!preg_match(regexfor('any'),$dond_estudia)){
  $_SESSION['error_entry_1338231974'] = 'corrija este campo';
    $failure = $failure +1;
  }
  if(!preg_match(regexfor('any'),$que_estudia)){
  $_SESSION['error_entry_446411834'] = 'corrija este campo';
    $failure = $failure +1;
  }
  if(!preg_match(regexfor('any'),$dond_trabaja)){
  $_SESSION['error_entry_1241364390'] = 'corrija este campo';
    $failure = $failure +1;
  }
  if(!preg_match(regexfor('any'),$profesion)){
  $_SESSION['error_entry_80365004'] = 'corrija este campo';
    $failure = $failure +1;
  }

  if(!preg_match(regexfor('any'),$sacramento)){
  $_SESSION['error_sacramento'] = 'corrija este campo';
    $failure = $failure +1;
  }

  if(!preg_match(regexfor(''),$o_catolica)){
  $_SESSION['error_entry_107097543'] = 'corrija este campo';
    $failure = $failure +1;
  }
  if(!preg_match(regexfor(''),$no_catolica)){
    $_SESSION['error_noCatolica'] = 'corrija este campo';
      $failure = $failure +1;
    }
  if(!preg_match(regexfor('ful-name'),$referido)){
  $_SESSION['error_entry_1204949772'] = 'corrija este campo';
    $failure = $failure +1;
  }

  if(!preg_match(regexfor('any'),$pre_misiones)){
  $_SESSION['error_misiones'] = 'corrija este campo';
    $failure = $failure +1;
  }
  if(!preg_match(regexfor('ful-name'),$novio)){
  $_SESSION['error_entry_992502456'] = 'corrija este campo';
    $failure = $failure +1;
  }
  if(!preg_match(regexfor('any'),$habilidades)){
  $_SESSION['error_habilidad'] = 'corrija este campo';
    $failure = $failure +1;
  }
  if(!preg_match(regexfor(''),$o_habilidades)){
  $_SESSION['error_entry_1252199433'] = 'corrija este campo';
    $failure = $failure +1;
  }

  // if(!preg_match(regexfor('fulname'),$tipo_sangre)){
  // $_SESSION['error_entry_507951984'] = 'corrija este campo';
  //   $failure = $failure +1;
  // }
  if(!preg_match(regexfor(''),$enfermedad)){
  $_SESSION['error_entry_1907784252'] = 'corrija este campo';
    $failure = $failure +1;
  }
  if(!preg_match(regexfor(''),$medicamento)){
  $_SESSION['error_entry_2069387399'] = 'corrija este campo';
    $failure = $failure +1;
  }
  if(!preg_match(regexfor(''),$alergia)){
  $_SESSION['error_entry_1262205387'] = 'corrija este campo';
    $failure = $failure +1;
  }

  if(!preg_match(regexfor('ful-name'),$papa_fulname)){
  $_SESSION['error_entry_704382283'] = 'corrija este campo';
    $failure = $failure +1;
  }
  if(!preg_match(regexfor('any'),$papa_tel)){
  $_SESSION['error_entry_147384584'] = 'corrija este campo';
    $failure = $failure +1;
  }
  if(!preg_match(regexfor('ful-name'),$mama_fulname)){
  $_SESSION['error_entry_1941103996'] = 'corrija este campo';
    $failure = $failure +1;
  }
  if(!preg_match(regexfor('any'),$mama_tel)){
  $_SESSION['error_entry_1126146889'] = 'corrija este campo';
    $failure = $failure +1;
  }
  if(!preg_match(regexfor(''),$disp_carro)){
  $_SESSION['error_entry_1560839713'] = 'corrija este campo';
    $failure = $failure +1;
  }
  if(!preg_match(regexfor(''),$emigrar)){
    $_SESSION['error_emigrar'] = 'corrija este campo';
      $failure = $failure +1;
    }



  // echo '<div style="border-bottom: 1px solid orange;" width = 100%> ------------- </div>';
  // echo "<h1> Tengo $failure errores DESPUES de validation </h1>";

$_SESSION['msg'] = '<p class="required-message" style="color:#f44336; font-weight: bold;">Existen '.$failure.' errores.  Por favor, revise los campos.</p>';


if( $failure == 0){
  // echo '<div style="border-bottom: 1px solid yellow;" width = 100%> ------------- </div>';
  // echo "<h1>Here: info clean and ready to go to DB</h1>";



  if($insertUpdate==0)//this is insert
  {

    $sql1 = "INSERT IGNORE INTO usuario (usr_id, primer_name, segun_name, primer_last, segun_last, "
    ."dob, rep_name, rep_tel, rep_parentesco, sexo, talla, calle_dir, tel_fijo, tel_mobil)"
    ." VALUES ( '$usr_id' ,'$name1' , '$name2' , '$last1', '$last2' , '$dob', '$rep_nom', '$rep_tel',"
    ."'$rep_parentesco', '$sexo' , '$talla' , '$direccion' ,'$tel_fijo','$tel_mobil' ); ";
    $mysqli->query($sql1) or die($mysqli->error);

    $sql1 = "INSERT IGNORE INTO salud (usr_id, sangre, enfermedad, medicina, alergia)"
    ." VALUES ( '$usr_id' ,'$tipo_sangre','$enfermedad','$medicamento','$alergia'); ";
    $mysqli->query($sql1) or die($mysqli->error);

    $sql1 = "INSERT IGNORE INTO personal (usr_id, novio_a, talentos, otro_tal, vehiculo, emigrar)"
    ." VALUES ( '$usr_id' ,'$novio' , '$habilidades' , '$o_habilidades', '$disp_carro','$emigrar'); ";
    $mysqli->query($sql1) or die($mysqli->error);

    $sql1 = "INSERT IGNORE INTO emergencia (usr_id, full_nombre_p, telefono_p, full_nombre_m, telefono_m)"
    ." VALUES ( '$usr_id' ,'$papa_fulname' , '$papa_tel' , '$mama_fulname', '$mama_tel'); ";
    $mysqli->query($sql1) or die($mysqli->error);

    $sql1 = "INSERT IGNORE INTO educacion (usr_id, estudia_donde, estudia_que, trabaja_donde, profesion, "
    ."sacramentos, grupos_catolic, grupos_nocatolic, referidode, misiones)"
    ." VALUES ( '$usr_id' ,'$dond_estudia' , '$que_estudia' , '$dond_trabaja', '$profesion' , '$sacramento', "
    ."'$o_catolica' , '$no_catolica' , '$referido' , '$pre_misiones' ); ";
    $mysqli->query($sql1) or die($mysqli->error);


    //now the important stuff I need to check
    $sql1 = "INSERT IGNORE INTO descarga (usr_id, actualizacion) VALUES ( '$usr_id' ,'1' ); ";
    $mysqli->query($sql1) or die($mysqli->error);


    // echo '<div style="border-bottom: 1px solid green;" width = 100%> ------------- </div>';
    // echo 'salir de todo.. logout y demas...' ;
    $_SESSION['msg'] = atento('guardado');
    $mysqli->close();
    redirect(0,"session.php");
    exit;
  }
  else if($insertUpdate==1) //this is update
  {
    $sql1 = "UPDATE usuario SET primer_name = '$name1' , segun_name = '$name2' , primer_last = '$last1', "
    ."segun_last = '$last2' , dob = '$dob' , rep_name = '$rep_nom' , rep_tel = '$rep_tel' , "
    ."rep_parentesco = '$rep_parentesco' , sexo = '$sexo' , talla = '$talla' , "
    ."calle_dir = '$direccion' , tel_fijo = '$tel_fijo' , tel_mobil = '$tel_mobil' "
    ."WHERE usr_id='$usr_id'; ";
    $mysqli->query($sql1) or die($mysqli->error);

    $sql1 = "UPDATE salud SET sangre = '$tipo_sangre', enfermedad = '$enfermedad', medicina = '$medicamento', alergia = '$alergia'"
    ."WHERE usr_id='$usr_id'; ";
    $mysqli->query($sql1) or die($mysqli->error);

    $sql1 = "UPDATE personal SET novio_a = '$novio', talentos = '$habilidades', otro_tal = '$o_habilidades', vehiculo = '$disp_carro', emigrar = '$emigrar'"
    ."WHERE usr_id='$usr_id'; ";
    $mysqli->query($sql1) or die($mysqli->error);

    $sql1 = "UPDATE emergencia SET full_nombre_p = '$papa_fulname', telefono_p = '$papa_tel', full_nombre_m = '$mama_fulname', telefono_m = '$mama_tel'"
    ."WHERE usr_id='$usr_id'; ";
    $mysqli->query($sql1) or die($mysqli->error);

    $sql1 = "UPDATE educacion SET estudia_donde = '$dond_estudia', estudia_que = '$que_estudia', trabaja_donde = '$dond_trabaja', profesion = '$profesion', "
    ."sacramentos = '$sacramento', grupos_catolic = '$o_catolica', grupos_nocatolic = '$no_catolica', referidode = '$referido', misiones = '$pre_misiones'"
    ."WHERE usr_id='$usr_id'; ";
    $mysqli->query($sql1) or die($mysqli->error);


    //now the important stuff I need to check
    $sql1 = "UPDATE descarga SET actualizacion=1 WHERE usr_id='$usr_id'; ";
    $mysqli->query($sql1) or die($mysqli->error);

   $_SESSION['msg'] = atento('actualizado');
  //  echo '<br/>';
  //  echo $_SESSION['msg'];
    $mysqli->close();
    redirect(0,"session.php");
    exit;

  }

  }else{
// echo '<p style="color:red;" >corrija la forma</p>';
  // $msg = "Porfavor corrija la forma";
  $mysqli->close();
  redirect(0,"fillup.php");
  exit;
  }

}else{
redirect(0,"fillup.php");
exit;
// echo "a message that you need to fill up the whole form";

 }
}
