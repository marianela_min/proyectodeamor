<?php
require_once "repeat.php";
if(!isset($_SESSION)) session_start();

// echo '<pre> SESSION____FIRST: ';
// echo var_dump($_SESSION);
// echo '</pre>';  

session_unset();
// echo '<pre> SESSION____AFTER UNSET: ';
// echo var_dump($_SESSION);
// echo '</pre>';  

session_destroy();

// echo '<pre> SESSION____AFTER DESTROY: ';
// echo var_dump($_SESSION);
// echo '</pre>';  

redirect(0,"login.php");
