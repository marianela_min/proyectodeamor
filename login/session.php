<?php

require_once "repeat.php";
require_once "../messages/messages.php";
require_once "../inc/db_mngmt.php";
require_once "../model/data.php";

if(!isset($_SESSION))
    session_start();

if(!isset($_SESSION['email']) || !isset($_SESSION['pass']))
    redirect(0,"login.php");

$email = $_SESSION["email"];
$ci  = $_SESSION['cedula'];
$id = $_SESSION["usr_id"];
$hash = $_SESSION["pass"];
$msg = $_SESSION["msg"];
$msg3 = ""; // mensaje de deuda!

$loggedInTime = $_SESSION["loggedIn"];
// print_r($_SESSION);

require_once "server.php";

// debe dinero?
$solvente = estaSolvente($ci,$mysqli);

// el usuario existe en la tabla de usuarios?
$sql = "SELECT usr_id FROM usuario WHERE usr_id='$id'; ";
$result = $mysqli->query($sql);

if($result->num_rows != 0){
    // usuario existe y esta registrado en el sistema!
    $needToFillUserData = false;
    $_SESSION['insertUpdate'] = 1;
    $row = $result->fetch_assoc();
    // asistir a misiones solamente
    // $asistirEventos['asistir_mision'] = intval($row['asistir_mision']);

    // asistir a las mini misiones solamente
    // TODO: get this info from the database intval($row['minimisiones']);
    // asistirEventos['identificador del evento'] =  1
    $asistirEventos['minimisiones'] = intval(0);
    $asistirEventos['misiones2020'] = intval(0);

    // $shortcutAsistir = $row['asistir_mision'];
        $sql = "SELECT misioneroID, eventoID FROM eventoPago WHERE misioneroID='$id'; ";
    // $sql = "SELECT misioneroID, eventos.eventoID, eventos.nombreEvento FROM eventoPago INNER JOIN eventos ON misioneroID='$id'; ";
    $result = $mysqli->query($sql);
    $shortcutAsistir = 0;
    if($result->num_rows != 0){
      while($row = $result->fetch_assoc())
      {
        // TODO: aca toca cambiar el numero 1 y 2
        // dependiendo del evento al que esta asistiendo
        if(intval($row['eventoID']) == 1){
          $asistirEventos['minimisiones'] = intval(1);
        }else if(intval($row['eventoID']) == 2){
          $asistirEventos['misiones2020'] = intval(1);
        }
      }
    }
    if(!$solvente){
      //assistir a Mini Misiones
        // $msg3 .= estadoDeSolvencia($solvente, $eventosPDA['minimisiones19']['name']);
      //assistir a Misiones
      $msg3 .= estadoDeSolvencia($solvente,$eventosPDA['misiones2020']['name']);
    }

}else{
    $needToFillUserData = true;
    $_SESSION['insertUpdate'] = 0;
}

$_SESSION['needToFill']=$needToFillUserData;
$_SESSION['solvente'] = $solvente;
$msg2 = sessionMsg($needToFillUserData);


//authorize to download or not? and get type of misionero this person is
$sql = "SELECT privilegio, misionero FROM login WHERE usr_id='$id'; ";
$result = $mysqli->query($sql);
if($result->num_rows==0){

    //no hay usuario aca todavia
    $descargaBtn = '<span></span>';
    $misioneroNuevoViejo = 0;

}else{

    $row = $result->fetch_assoc();
    $usrPrivilegio = intval($row['privilegio']);
    $misioneroNuevoViejo=intval($row['misionero']);
    $_SESSION['userPrivilegio'] = $usrPrivilegio;
    if($usrPrivilegio === 5){
        if(isset($_GET['descarga'])){
            if(intval($_GET['descarga']) == 0){ //todo
                $descargaBtn = descargaDatos($mysqli,true);
            }else if(intval($_GET['descarga']) == 1){ //lo nuevo
                $descargaBtn = descargaDatos($mysqli,false);
            }
        }else{
            $descargaBtn = '<a type="button" class="btn btn-info" href="session.php?descarga=0"><i class="fas fa-download"></i> Todo</a>
            <a type="button" class="btn btn-info" href="session.php?descarga=1"><i class="fas fa-download"></i> Sólo nuevos cambios</a>';
        }
        // $descargaBtn .= ' <a type="button" class="btn btn-secondary" href="insertDeleteCi.php"><i class="far fa-edit"></i> Morosos</a>';
        $descargaBtn .= ' <a type="button" class="btn btn-secondary" href="unconfirmed.php"><i class="far fa-edit"></i> Modificar Usuarios</a>';
    }else{
        $descargaBtn = '<span></span>';
    }
}


$_SESSION['tipo_misionero'] = $misioneroNuevoViejo;
$eventoID = 1;
$cupos = ($misioneroNuevoViejo==0)? getInscritos($mysqli, $eventoID): 10;
// echo "Quedan ".(750-intval($cupos))." cupos";
$_SESSION['inscripcionAbierta'] =  fechaRange($misioneroNuevoViejo, $cupos, $eventosPDA['misiones2020']);
// print_r($_SESSION['inscripcionAbierta']);
$listoParaInscribir = $_SESSION['inscripcionAbierta']['ready'];

$mysqli->close();


//if a valid user then I check for inactivity?
if(isset($_SESSION['usr_id'])){
    if(time() - $loggedInTime > 1800){ // time in seconds 1800 for 30min
        header("Location: logout.php");
    }else{
        $_SESSION["loggedIn"] = time();
    }
}

?>


<!DOCTYPE html>
<html>
<head>
    <?php include("../css/style_config.php") ?>
    <title>Proyecto de amor</title>
    <link rel="stylesheet" href="../css/session.css">
</head>
<body>
<nav class="nav-text naveg-bar custom-nav">
		<ul class="nav justify-content-between">
			<li>Hola <em><?= $email ?></em>,</li>
            <li><a type="button" class="btn btn-danger btn-sm" href="logout.php">Cerrar Sesión <i class="fas fa-sign-out-alt"></i></a></li>
        </ul>
        <div class="small text-right">Su sesión expirará luego de 30 min <span id='loggedIn'></span> de inactividad.</div>
</nav>

<div align="center">
    <a type="button" class="btn btn-warning btn-sm" href="session.php"><i class="fas fa-sync-alt"></i> Sesión</a>
    <?= $descargaBtn ?>
</div>

<main class="content-start">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 col-md-offset-3" align="center">
                <h1>Proyecto de Amor</h1>
                <h4>Bienvenido</h4>
                <img class="img-fluid" width="20%" src="../images/logo.png" alt="PDA logo"><br/><br/>
        </div>
    </div>
</div>

<!-- MESSAGES IN THE MIDDLE -->
<div class="container">
    <div class="row justify-content-center">
        <div class="" align="center">
                <?= $msg ?>
                <div class="col-md-10 text-centered"><?= $msg2?></div>
                <br/>
        </div>
    </div>
</div>


<!-- REPORTE PARA LUISRA O CUALQUIER OTRO ADMINISTRADOR-->
<?php
reporteDeInscripcionMsg($usrPrivilegio, $eventosPDA);
?>

<br/>
<br/>

<!-- INFORMACIONES PARA EL PUBLICO -->
<div class="container">
    <div class="row justify-content-around" align="left">
        <div class="col-md-8 main-message">
        <h3 class="inscripcion-day"><i class="fas fa-exclamation"></i> Información importante</h3 >
        <p>Inscripciones para asistir a las Misiones PDA 2020(*) se habilitarán de la siguiente manera:</p>
        <ul>
        <li>Del <span class="inscripcion-day">10 de Enero al 12 de Enero</span>, para quienes han misionado antes con PDA</li>
        <li>Del <span class="inscripcion-day">13 de Enero al 17 de Enero o hasta que se terminen los cupos</span>, para todos.</li>
        </ul></div>
    </div>
</div>
<br/>


<!-- ESTADO DEL USUARIO Y PROXIMOS EVENTOS-->
<div class="container" >
    <div class="row justify-content-around">
        <div class="col-sm-8 col-md-6">
            <h5>Estado del usuario:</h5>
                    <?php
                      if($solvente){

                              if($needToFillUserData){
                                  echo '<div style="display:flex; content-justify: space-around">';
                                  echo '<p style="color:red;">1.&#9;<span><i style="color:red;" class="col-1 fas fa-times"></i></span> Usted aún no ha ingresado sus datos en la planilla</p>';
                                  echo '<a href="fillup.php" class="btn btn-outline-primary col-md-6" tabindex="-1" role="button" aria-disabled="true">Ingresar<br>Datos</a> ';
                                  echo '</div>';
                              }else{
                                  echo '<p style="color:green;">1.&#9;<span><i style="color:green;" class="col-1 fas fa-check"></i></span> Ya llenó la planilla con sus datos personales</p>';
                                  echo '<a href="readUserDB.php" class="btn btn-outline-info col-md-3 " tabindex="-1" role="button" aria-disabled="true">Ver<br>Datos</a> ';
                                  echo '<a href="readUserDB.php?modif=1" class="btn btn-outline-warning col-md-4 col-lg-3" tabindex="-1" role="button" aria-disabled="true"><i class="far fa-edit"></i> Modificar<br>Datos</a> ';
                                  echo '<br><br>';
                              }
                      }else{
                          echo estadoDeSolvencia($solvente, $eventosPDA['misiones2020']['name']);
                      }

                    ?>
                    <br>
        </div>
        <div class="col-sm-8 col-md-6">
            <h5>Próximos Eventos:</h5>
                    <?php
                          // $evento = $eventosPDA['minimisiones19'];
                          // if($asistirEventos['minimisiones'] == 0){
                          //     echo '<p style="color:red;">1.&#9;<span><i style="color:red;" class="col-1 fas fa-times"></i></span> Aún no está inscrito para asistir a: '.$evento['name'].'</p>';
                          //     echo '<a href="'.$evento['inscripcionUrl'];
                          //     // echo 'registerEvent.php?event='.$eventCode;
                          //     echo '" class="btn btn-outline-';
                          //     //modify here for when the time to register comes...
                          //     echo ($listoParaInscribir == 1 && $solvente)? 'primary':'secondary disabled';
                          //     echo ' col-md-5 " tabindex="-1" role="button" aria-disabled="true"><i class="far fa-edit"></i> ';
                          //     echo ($cupos < $evento['maxCupos'])?'Inscripción<br/>'.$evento['name']:'Cupos<br/>Agotados';
                          //     echo '</a>';
                          //
                          // }else{
                          //     echo '<p style="color:green;">1.&#9;<span><i style="color:green;" class="col-1 fas fa-check"></i></span> Usted ya envió la solicitud para asistir a: '.$evento['name'].'</p>';
                          // }

                          // if($asistirEventos['asistir_mision'] == 0){
                          //   echo '<p style="color:red;">2.&#9;<span><i style="color:red;" class="col-1 fas fa-times"></i></span> Aún no está inscrito para asistir a: Misiones PDA 2020 (*)</p>';
                          // }else{
                          //   echo '<p style="color:green;">2.&#9;<span><i style="color:green;" class="col-1 fas fa-check"></i></span> Usted ya envió la solicitud para asistir a las Misiones PDA 2019 (*)</p>';
                          // }

                          $evento = $eventosPDA['misiones2020'];
                          if($asistirEventos['misiones2020'] == 0){
                              echo '<p style="color:red;">1.&#9;<span><i style="color:red;" class="col-1 fas fa-times"></i></span> Aún no está inscrito para asistir a: '.$evento['name'].'</p>';
                              echo '<a href="'.$evento['inscripcionUrl'];
                              // echo 'registerEvent.php?event='.$eventCode;
                              echo '" class="btn btn-outline-';
                              //modify here for when the time to register comes...
                              echo ($listoParaInscribir == 1 && $solvente)? 'primary':'secondary disabled';
                              echo ' col-md-5 " tabindex="-1" role="button" aria-disabled="true"><i class="far fa-edit"></i> ';
                              echo ($cupos < $evento['maxCupos'])?'Inscripción<br/>'.$evento['name']:'Cupos<br/>Agotados';
                              echo '</a>';

                          }else{
                              echo '<p style="color:green;">1.&#9;<span><i style="color:green;" class="col-1 fas fa-check"></i></span> Usted ya envió la solicitud para asistir a: '.$evento['name'].'</p>';
                          }

                    ?>

        </div>

    </div>

    <!-- DISCLAIMER -->
    <div class="row content-justified font-italic disclaimer-txt">
           <p>(*):</p>
           <?= disclaimerMsg();?>
    </div>
</div>


</main>
<?php include("../inc/footer.php") ?>
<script>
let minutes = 30;
let seconds = 0;
setInterval(() => {
  if(seconds == 0){
    if(minutes == 0){
      console.log('stop the counter'); //clearInterval();
    }else{
      seconds =59;
      minutes = minutes - 1;
    }
  }else{
    seconds = seconds - 1;
  }

  document.querySelector('#loggedIn').innerText = "(" + minutes + ":" + seconds + ")";
},1000);
</script>
</body>
</html>
<?php $_SESSION["msg"] = '';?>
