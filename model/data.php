<?php
require_once "../login/server.php";

$sql = "SELECT * FROM eventos";
$result = $mysqli->query($sql) or die($mysqli->error);


if($result->num_rows!=0){

  //TODO:  fetches only the first row, it needs to populate the entire events table
  while($row=$result->fetch_assoc())
    {
      // $row=$result->fetch_assoc();
      $id = $row['eventoID'];
      $name = $row['nombreEvento'];
      $inscripcionUrl = $row['inscripcionUrl'];
      $maxCupos = $row['maxCupos'];
      $openPublicationDate = new DateTime($row['openPublicationDate'], new DateTimeZone('America/Caracas'));
      $openRegDate = new DateTime($row['openRegDate'], new DateTimeZone('America/Caracas'));
      $closeRegDate = new DateTime($row['closeRegDate'], new DateTimeZone('America/Caracas'));
      $closePublicationDate = new DateTime($row['closePublicationDate'], new DateTimeZone('America/Caracas'));

      preg_match("/evento=(\w+)/",$inscripcionUrl,$matches);
      $identifier = $matches[1];
      $eventosPDA[$identifier] = [
                          "id" => $id,
                          "name" => $name,
                          "inscripcionUrl" => $inscripcionUrl,
                          "maxCupos"  => $maxCupos,
                          "openPublicationDate"  => $openPublicationDate,
                          "openRegDate"  => $openRegDate,
                          "closeRegDate"  => $closeRegDate,
                          "closePublicationDate"  => $closePublicationDate
                        ];
    }
    // echo '<pre>';
    // var_dump($eventosPDA);
    // echo '</pre>';
}

// $eventosPDA = [
//   $identifier => [
//                     "id" => $id,
//                     "name" => $name,
//                     "inscripcionUrl" => $inscripcionUrl,
//                     "maxCupos"  => $maxCupos,
//                     "openPublicationDate"  => $openPublicationDate,
//                     "openRegDate"  => $openRegDate,
//                     "closeRegDate"  => $closeRegDate,
//                     "closePublicationDate"  => $closePublicationDate,
//
//                   ],
//   "misiones20" => [
//                     "id" => 2,
//                     "name" => "Misiones 2020",
//                     "inscripcionUrl" => "registerEventos.php?evento=misiones20",
//                     "maxCupos"  => 750,
//                     "openPublicationDate"  => new DateTime('12/15/2019 23:59:00', new DateTimeZone('America/Caracas')),
//                     "openRegDate"  => new DateTime('01/07/2019 00:00:00', new DateTimeZone('America/Caracas')),
//                     "closeRegDate"  => new DateTime('01/16/2019 23:59:00', new DateTimeZone('America/Caracas')),
//                     "closePublicationDate"  => new DateTime('01/18/2020 23:59:00', new DateTimeZone('America/Caracas')),
//
//                   ]
// ];



// -- create the table eventos
// CREATE TABLE `eventos` ( `eventoID` INT NOT NULL AUTO_INCREMENT COMMENT 'id del evento' ,  `nombreEvento` VARCHAR(100) NOT NULL COMMENT 'nombre del evento' ,  `inscripcionUrl` VARCHAR(120) NOT NULL ,  `maxCupos` INT NOT NULL ,  `openPublicationDate` DATETIME NOT NULL ,  `openRegDate` DATETIME NOT NULL ,  `closeRegDate` DATETIME NOT NULL ,  `closePublicationDate` DATETIME NOT NULL ,    PRIMARY KEY  (`eventoID`)) ENGINE = InnoDB;


// -- ADD a new evento
// require_once "../login/server.php";
// $name = $mysqli->escape_string($eventosPDA['minimisiones19']['name']);
// $inscripcionUrl = $mysqli->escape_string($eventosPDA['minimisiones19']['inscripcionUrl']);
// $maxCupos = $mysqli->escape_string($eventosPDA['minimisiones19']['maxCupos']);
// $openPublicationDate = $eventosPDA['minimisiones19']['openPublicationDate']->format('Y-m-d H:i:s');
// $openRegDate = $eventosPDA['minimisiones19']['openRegDate']->format('Y-m-d H:i:s');
// $closeRegDate = $eventosPDA['minimisiones19']['closeRegDate']->format('Y-m-d H:i:s');
// $closePublicationDat = $eventosPDA['minimisiones19']['closePublicationDate']->format('Y-m-d H:i:s');
//
// $sql = "INSERT INTO eventos (nombreEvento, inscripcionUrl, maxCupos, openPublicationDate, openRegDate, closeRegDate, closePublicationDate) VALUES (
//   '$name',
//   '$inscripcionUrl',
//   $maxCupos,
//   '$openPublicationDate',
//   '$openRegDate',
//   '$closeRegDate',
//   '$closePublicationDat');
// ";
//
// echo $sql;
// $mysqli->query($sql) or die($mysqli->error);



//create the table tranasaction
// CREATE TABLE `eventoPago` (
//   `misioneroID` INT NOT NULL ,
//   `eventoID` INT NOT NULL ,
//   `fechaPago` DATE NOT NULL ,
//   `nombreBanco` VARCHAR(100) NOT NULL DEFAULT "N/A",
//   `cedulaTitular` VARCHAR(100) NOT NULL DEFAULT "N/A",
//   `nroReferencia` VARCHAR(100) NOT NULL DEFAULT "N/A",
//   `tipoMoneda` VARCHAR(100) NOT NULL DEFAULT "N/A",
//   `monto` DECIMAL(20,8) NOT NULL ,
//   `kilo` VARCHAR(100) NOT NULL COMMENT 'compromiso de pago en comida' ,
//   PRIMARY KEY (`misioneroID`, `eventoID`),
//   FOREIGN KEY(`eventoID`) REFERENCES eventos(`eventoID`)
//   --FOREIGN KEY(`p_misioneroID`) REFERENCES login(`usr_id`)
//   ) ENGINE=MyISAM DEFAULT CHARSET=utf8;


// TODO: this doesnot work I dont kow why <-- because you should use different names for primary keys
// alter table eventoPago
// FOREIGN KEY (`misioneroID`) REFERENCES login(`usr_id`)


// INSERT IGNORE INTO `eventoPago` (misioneroID, eventoID, fechaPago, nombreBanco, cedulaTitular, nroReferencia, tipoMoneda, monto, kilo)
// VALUES (61, 1, '2020-07-26', 'Efectivo',	'Efectivo',	'Efectivo',	'USD',	10,	'Arroz'),
// (182, 1, '2019-11-26', 'Banco Exterior', 'Barbara Claros',	'687251', 'Bs.',	354000,	'harina'),
// (813, 1, '2019-11-26', 'Exterior',	'Sarah Claros' , '203835', 'Bs.',	370000,	'Harina'),
// (112, 1, '2019-11-28', 'Banco del Caribe',	'Laura Delgado' , '845511	', 'Bs.',	404000,	'Azúcar'),
// (38, 1, '2019-11-24', 'Bofa Rosa',	'Bofa Rosa', 'Bofa Rosa', 	'USD',	10,	'Arroz'),
// (172, 1, '2019-11-26', 'efectivo', 	'efectivo', 	'efectivo', 	'USD', 10,	'harina pan'),
// (378, 1, '2019-12-01', 'Efectivo',	'Efectivo',	'Efectivo',	'USD', 10,	'Arroz'),
// (124, 1, '2019-11-26', 'provincial', 'pago movil Provincial'  , '3137', 'Bs.',	370000,	'harina'),
// (32, 1, '2019-11-24', 'Zelle',	'Jorge Alvarez', '1024637761',	'USD', 10,	'Pasta'),
// (33, 1, '2019-11-30', 'Banesco',	'26540901' , '2657125527', 'Bs.',	395000,	'Harina'),
// (379, 1, '2019-12-01', 'Provincial',	'26121882' , '42231031', 'Bs.',	395000,	'harina'),
// (509, 1, '2019-11-26', 'Banco de Venezuela',	'9619819' , '51340976', 'Bs.',	360000,	'Arroz'),
// (114, 1, '2019-11-24', 'Exterior', 	'9614512', '755493', 'Bs.',	338000,	'Harina '),
// (93, 1, '2019-11-25', 'Bank of America',	'Iris Sandoval',	'Iris Sandoval',	'USD', 10,	'Pasta'),
// (102, 1, '2019-11-26', 'Bank of America',	'Gustavo Escalona',	'062c94f51',	'USD', 10,	'Harina'),
// (120, 1, '2019-12-02', 'Banesco',	'26540901' , '2659052066', 'Bs.',	395000,	'Harina '),
// (6, 1, '2019-11-23', 'Mercantil',	'11878770' , '84730658876', 'Bs.',	676,	'Harina de Maiz'),
// (413, 1, '2019-11-24', 'MERCANTIL',	'MARIELA MOTTOLA' , '84730658876', 'Bs.',	676000,	'HARINA DE MAIZ'),
// (342, 1, '2019-11-26', 'N/a', 	'Mariana Salazar', 	'174', 'USD', 10, 'Harina' ),
// (83, 1, '2019-12-14', 'n/a','	no',	'no',	'USD', 10,	'harina'),
// (331, 1, '2019-12-02', 'paypal', 'Luis Torres',	'2FE48736ED2049820',	'USD', 10,	'arroz'),
// (3, 1, '2019-11-24', 'Bank of America',	'Luis Vasquez',	'Propio',	'USD', 10,	'Azucar'),
// (234, 1, '2019-11-24', 'Provincial',	'12704988', '41331583', 'Bs.',	338,	'Arroz')




?>
