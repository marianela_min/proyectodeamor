<div class="container">
    <div class="row content-justified font-italic">
        <div class="col-12">
               <p>Por favor coloque la informacion de pago para formalizar su inscripción en: <?= $eventosPDA[$keyEvento]['name'] ?></p>
        </div>
        <div class="col-12" align="left">
            <form method="POST" action="">
            <input type="text" value=<?=$id?> hidden>

            <div class="ss-form-question errorbox-good" role="listitem">
              <div dir="auto" class="ss-item ss-item-required ss-date">
                <div class="ss-form-entry">
                  <label class="ss-q-item-label">
                    <div class="ss-q-title">Fecha de pago
                      <!-- <label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label> -->
                      <span class="ss-required-asterisk" aria-hidden="true">*</span>
                    </div>
                    <div class="ss-q-help ss-secondary-text" dir="auto">Coloque la fecha de pago en formato MES/DIA/AÑO</div>
                  </label>
                  <?php $da = (new DateTime('now', new DateTimeZone('America/Caracas')))->format('d-m-Y') ?>
                  <input type="date" name="in_fechaPago" oninput="isvalid(this.id,'dob')" value="<?= isset($_SESSION['in_fechaPago'])? htmlentities($_SESSION['in_fechaPago']) : $da ?>" class="ss-q-date" dir="auto" id="in_fechaPago" aria-label="Fecha de Pago" aria-required="true" >
                  <span class="required-message" ></span>

                  <br/>
                  <div class="error-message" id="in_fechaPago_errorMessage"></div>
                  <div class="required-message" hidden>Campo requerido</div>
                </div>
              </div>
            </div>


            <div class="ss-form-question errorbox-good" role="listitem">
              <div dir="auto" class="ss-item ss-item-required ss-text">
                <div class="ss-form-entry">
                  <label class="ss-q-item-label" for="in_cedula">
                    <div class="ss-q-title">Banco
                    <span class="ss-required-asterisk" aria-hidden="true">*</span>
                    </div>
                    <div class="ss-q-help ss-secondary-text" dir="auto">Nombre del banco donde se realizó el pago</div>
                  </label>
                  <input type="text" name="in_banco" oninput="isvalid(this.id,'any')"  value="" class="ss-q-short" id="in_banco" dir="auto" aria-label="Cedula del titular de la cuenta" aria-required="true"  step="any" title="Cedula del titular de la cuenta">
                  <span class="required-message" ></span>
                  <br/>
                  <div class="error-message" id="in_banco_errorMessage"></div>
                  <div class="required-message" hidden>Campo requerido</div>
                </div>
              </div>
            </div>

            <div class="ss-form-question errorbox-good" role="listitem">
              <div dir="auto" class="ss-item ss-item-required ss-text">
                <div class="ss-form-entry">
                  <label class="ss-q-item-label" for="in_cedula">
                    <div class="ss-q-title">Contesta según haya sido tu método de pago:
                    <!-- <label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label> -->
                    <span class="ss-required-asterisk" aria-hidden="true">*</span>
                    </div>
                    <div class="ss-q-help ss-secondary-text" dir="auto">Transferencia en Bs.: Cédula de Identidad del titular de la cuenta / <br>Pago Movil: Número de Celular del titular de la cuenta / <br>Transferencia en USD: Nombre y Apellido del titular de la cuenta</div>
                  </label>
                  <input type="text" name="in_cedula" oninput="isvalid(this.id,'any')"  value="" class="ss-q-short" id="in_cedula" dir="auto" aria-label="Nombre o Cedula del titular de la cuenta" aria-required="true" title="Nombre o Cedula del titular de la cuenta">
                  <span class="required-message" ></span>
                  <br/>
                  <div class="error-message" id="in_cedula_errorMessage"></div>
                  <div class="required-message" hidden>Campo requerido</div>
                </div>
              </div>
            </div>

            <div class="ss-form-question errorbox-good" role="listitem">
              <div dir="auto" class="ss-item ss-item-required ss-text">
                <div class="ss-form-entry">
                  <label class="ss-q-item-label" for="in_cedula">
                    <div class="ss-q-title">Numero de Referencia
                    <span class="ss-required-asterisk" aria-hidden="true">*</span>
                    </div>
                    <div class="ss-q-help ss-secondary-text" dir="auto"></div>
                  </label>
                  <input type="text" name="in_referencia" oninput="isvalid(this.id,'any')" value="" class="ss-q-short" id="in_referencia" dir="auto" aria-label="Referencia" aria-required="true"  title="Referencia">
                  <span class="required-message" ></span>
                  <br/>
                  <div class="error-message" id="in_referencia_errorMessage"></div>
                  <div class="required-message" hidden>Campo requerido</div>
                </div>
              </div>
            </div>

            <div class="ss-form-question errorbox-good" role="listitem">
              <div dir="auto" class="ss-item ss-item-required ss-text">
                <div class="ss-form-entry">
                  <label class="ss-q-item-label" for="in_cedula">
                    <div class="ss-q-title">Moneda (Bs., USD, BTC)
                    <span class="ss-required-asterisk" aria-hidden="true">*</span>
                    </div>
                    <div class="ss-q-help ss-secondary-text" dir="auto">Moneda en la que se realizo el pago</div>
                  </label>
                  <!-- <label>
                    Month: -->
                    <select name="in_moneda" value="" class="ss-q-short" id="in_moneda" dir="auto" aria-label="Moneda" aria-required="true"  step="any" title="Moneda">
                      <option value="Bs.">Bs.</option>
                      <option value="USD">USD</option>
                      <option value="BTC">BTC</option>
                    </select>
                  <!-- </label> -->
                  <!-- <input type="text" name="in_moneda" oninput="isvalid(this.id,'any')" value="" class="ss-q-short" id="in_moneda" dir="auto" aria-label="Moneda" aria-required="true"  step="any" title="Moneda"> -->
                  <span class="required-message" ></span>
                  <br/>
                  <div class="error-message" id="in_moneda_errorMessage"></div>
                  <div class="required-message" hidden>Campo requerido</div>
                </div>
              </div>
            </div>

            <div class="ss-form-question errorbox-good" role="listitem">
              <div dir="auto" class="ss-item ss-item-required ss-text">
                <div class="ss-form-entry">
                  <label class="ss-q-item-label" for="in_cedula">
                    <div class="ss-q-title">Monto
                    <span class="ss-required-asterisk" aria-hidden="true">*</span>
                    </div>
                    <div class="ss-q-help ss-secondary-text" dir="auto"></div>
                  </label>
                  <input type="text" name="in_monto" oninput="isvalid(this.id,'any')" value="" class="ss-q-short" id="in_monto" dir="auto" aria-label="Monto" aria-required="true"  step="0.000000000000001" title="Monto">
                  <span class="required-message" ></span>
                  <br/>
                  <div class="error-message" id="in_monto_errorMessage"></div>
                  <div class="required-message" hidden>Campo requerido</div>
                </div>
              </div>
            </div>

            <div class="ss-form-question errorbox-good" role="listitem">
              <div dir="auto" class="ss-item ss-item-required ss-text">
                <div class="ss-form-entry">
                  <label class="ss-q-item-label" for="in_cedula">
                    <div class="ss-q-title">Kilo que aportarás
                    <span class="ss-required-asterisk" aria-hidden="true">*</span>
                    </div>
                    <div class="ss-q-help ss-secondary-text" dir="auto">Coloca de que será el kilo que aportarás para formalizaar tu inscripción. Ej: Harina, arroz, aceite, leche, avena, azúcar, margarina, etc... </div>
                  </label>
                  <input type="text" name="in_kilo" oninput="isvalid(this.id,'any')" value="" class="ss-q-short" id="in_kilo" dir="auto" aria-label="Kilo de aporte" aria-required="true"  title="Kilo de aporte">
                  <span class="required-message" ></span>
                  <br/>
                  <div class="error-message" id="in_kilo_errorMessage"></div>
                  <div class="required-message" hidden>Campo requerido</div>
                </div>
              </div>
            </div>


            <input type="submit" name="submit" value="Enviar" id="ss-submit" class="btn btn-primary"/>
            <input type="submit" name="cancel" value="No Enviar" id="ss-cancel" class="btn btn-secondary"/>
            </form>
        </div>
    </div>
</div>
