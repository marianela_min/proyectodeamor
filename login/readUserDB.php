<?php
if(!isset($_SESSION)) session_start();
require_once "repeat.php";

if(!isset($_SESSION['email']) || !isset($_SESSION['pass']))
    redirect(0,"login.php");
    
    $email = $_SESSION["email"];
    $ci  = $_SESSION['cedula'];
    $id = $_SESSION["usr_id"];
    $hash = $_SESSION["pass"];
    $msg = $_SESSION["msg"];
    $loggedInTime = $_SESSION["loggedIn"];
    //print_r($_SESSION);
    // echo 'this is msg: '.$msg;

    
if(isset($_SESSION['usr_id']))
{
    if(time() - $loggedInTime > 1800) // time in seconds 1800 for 30min
    {
        header("Location: logout.php");
    }
    else{
        $_SESSION["loggedIn"] = time();
    }
}


require ("server.php");

$sql = "SELECT * FROM usuario WHERE usr_id='$id'; ";
$result = $mysqli->query($sql);
// print_r($result);
if($result->num_rows!=0){
    $row =  $result->fetch_assoc();
    
    $name1  = $_SESSION['entry_2055165912'] = $row['primer_name'];
    $name2  = $_SESSION['entry_498994277'] = $row['segun_name'];
    $last1  = $_SESSION['entry_359752058'] = $row['primer_last'];
    $last2  = $_SESSION['entry_119140095'] = $row['segun_last'];
    $dob  = $_SESSION['entry_2101364991'] = $row['dob'];
    $rep_nom = $_SESSION['RepNombre'] = $row['rep_name'];
    $rep_tel = $_SESSION['RepTel'] = $row['rep_tel'];
    $rep_parentesco = $_SESSION['RepParentesco'] = $row['rep_parentesco'];
    // $ci  = $_SESSION['entry_2005063964'] = $row['cedula'];
    $sexo  = $_SESSION['entry_2128285595'] = $row['sexo'];
    $talla  = $_SESSION['entry_1613747071'] = $row['talla'];
    $direccion  = $_SESSION['entry_514490750'] = $row['calle_dir'];
    $tel_fijo  = $_SESSION['entry_1309052470'] = $row['tel_fijo'];
    $tel_mobil  = $_SESSION['entry_1677385248'] = $row['tel_mobil'];
        
}

   
$sql = "SELECT * FROM salud WHERE usr_id='$id'; ";
$result = $mysqli->query($sql);
// print_r($result);
if($result->num_rows!=0){
    $row =  $result->fetch_assoc();
    $tipo_sangre  = $_SESSION['entry_507951984'] = $row['sangre'];
    $enfermedad  = $_SESSION['entry_1907784252'] = $row['enfermedad'];
    $medicamento  = $_SESSION['entry_2069387399'] = $row['medicina'];
    $alergia  = $_SESSION['entry_1262205387'] = $row['alergia'];       
}

$sql = "SELECT * FROM personal WHERE usr_id='$id'; ";
$result = $mysqli->query($sql);
// print_r($result);
if($result->num_rows!=0){
    $row =  $result->fetch_assoc();
 
    $novio  = $_SESSION['entry_992502456'] = $row['novio_a'];
    $habilidades  = $_SESSION['habilidad'] = $row['talentos'];
    $o_habilidades  = $_SESSION['entry_1252199433'] = $row['otro_tal'];
    $disp_carro  = $_SESSION['entry_1560839713'] = $row['vehiculo'];
    $emigrar  = $_SESSION['emigrar'] = $row['emigrar'];

}

$sql = "SELECT * FROM emergencia WHERE usr_id='$id'; ";
$result = $mysqli->query($sql);
// print_r($result);
if($result->num_rows!=0){
    $row =  $result->fetch_assoc();
    
    $papa_fulname  = $_SESSION['entry_704382283'] = $row['full_nombre_p'];
    $papa_tel  = $_SESSION['entry_147384584'] = $row['telefono_p'];
    $mama_fulname  = $_SESSION['entry_1941103996'] = $row['full_nombre_m'];
    $mama_tel  = $_SESSION['entry_1126146889'] = $row['telefono_m'];
}

$sql = "SELECT * FROM educacion WHERE usr_id='$id'; ";
$result = $mysqli->query($sql);
// print_r($result);
if($result->num_rows!=0){
    $row =  $result->fetch_assoc();
      
    $dond_estudia  = $_SESSION['entry_1338231974'] = $row['estudia_donde'];
    $que_estudia  = $_SESSION['entry_446411834'] = $row['estudia_que'];
    $dond_trabaja  = $_SESSION['entry_1241364390'] = $row['trabaja_donde'];
    $profesion  = $_SESSION['entry_80365004'] = $row['profesion'];
    $sacramento  = $_SESSION['sacramento'] = $row['sacramentos'];
    $o_catolica  = $_SESSION['entry_107097543'] = $row['grupos_catolic'];
    $no_catolica = $_SESSION['noCatolica'] = $row['grupos_nocatolic'];
    $referido  = $_SESSION['entry_1204949772'] = $row['referidode'];
    $pre_misiones  = $_SESSION['misiones'] = $row['misiones'];

}

$_SESSION['msg'] = "";

if(isset($_GET['modif']))
  redirect(0,"fillup.php");

?>
<!DOCTYPE html>
<html lang="es">
<head>
<?php require_once ("../css/style_config.php"); ?>
<title> PdA - Proyecto de Amor </title>
</head>

<body id="ver" >
<nav class="nav-text naveg-bar custom-nav">
		<ul class="nav justify-content-between">
			<li>Hola <em><?= $email ?></em>,</li>
            <li><a type="button" class="btn btn-danger btn-sm" href="logout.php">Cerrar Sesión <i class="fas fa-sign-out-alt"></i></a></li>
        </ul>
        <div class="small text-right">Su sesión expirará luego de <span id='loggedIn'>30 min</span> de inactividad.</div>
</nav>

<div align="center">
<a type="button" class="btn btn-warning btn-sm" href=""><i class="fas fa-sync-alt"></i> Sesión</a>
</div>

<main class="content-start ss-base-body">

<div class="container">
<div class="row justify-content-center">
<div class="col-md-11" >


<div class="ss-form-container"><div class="ss-header-image-container"><div class="ss-header-image-image"><div class="ss-header-image-sizer"></div></div></div>
<div class="ss-top-of-page"><div class="ss-form-heading"><h1 class="ss-form-title" dir="ltr">Verificación de Datos</h1>
<img class="img-fluid" width="20%" src="../images/logo.png" alt="PDA logo"><br/><br/>
<div class="ss-form-desc ss-no-ignore-whitespace" dir="ltr"></div></div></div>


<div class="ss-form">
<form action="" method="POST" id="ss-form" target="_self" autocomplete="on" _lpchecked="1"><ol role="list" class="ss-question-list" style="padding-left: 0;">
<input type="number" name="id" id="usr_id" value="<?=$id?>" hidden>

<div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_2055165912"><div class="ss-q-title">1er Nombre
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto"></div></label>

<input readonly type="text" name="entry.2055165912" oninput="isvalid(this.id,'name')" value="<?=isset($_SESSION['entry_2055165912']) ? htmlentities($_SESSION['entry_2055165912']) : '';?>" class="ss-q-short " id="entry_2055165912" dir="auto" aria-label="1er Nombre  " aria-required="true" required="" title="" ><span class="required-message" ><?= isset($_SESSION['error_entry_2055165912']) ? htmlentities($_SESSION['error_entry_2055165912']) : ''; ?></span><br/>
<div class="error-message" id="1418297321_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_498994277"><div class="ss-q-title">2do Nombre
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto"></div></label>

<input readonly type="text" name="entry.498994277" oninput="isvalid(this.id,'name')" value="<?= isset($_SESSION['entry_498994277'])? htmlentities($_SESSION['entry_498994277']) : '' ?>" class="ss-q-short" id="entry_498994277" dir="auto" aria-label="2do Nombre  " aria-required="true" required="" title=""><span class="required-message" ><?=  isset($_SESSION['error_entry_498994277'])? htmlentities($_SESSION['error_entry_498994277']) : ''  ?></span><br/>
<div class="error-message" id="601044622_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_359752058"><div class="ss-q-title">1er Apellido
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto"></div></label>

<input readonly type="text" name="entry.359752058" oninput="isvalid(this.id,'name')" value="<?= isset($_SESSION['entry_359752058'])? htmlentities($_SESSION['entry_359752058']) : '' ?>" class="ss-q-short" id="entry_359752058" dir="auto" aria-label="1er Apellido  " aria-required="true" required="" title=""><span class="required-message" ><?=  isset($_SESSION['error_entry_359752058'])? htmlentities($_SESSION['error_entry_359752058']) : ''  ?></span><br/>
<div class="error-message" id="974870646_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_119140095"><div class="ss-q-title">2do Apellido
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto"></div></label>

<input readonly type="text" name="entry.119140095" oninput="isvalid(this.id,'name')" value="<?= isset($_SESSION['entry_119140095'])? htmlentities($_SESSION['entry_119140095']) : '' ?>" class="ss-q-short" id="entry_119140095" dir="auto" aria-label="2do Apellido  " aria-required="true" required="" title=""><span class="required-message" ><?=  isset($_SESSION['error_entry_119140095'])? htmlentities($_SESSION['error_entry_119140095']) : ''  ?></span><br/>
<div class="error-message" id="503513345_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_2005063964"><div class="ss-q-title">Cédula de Identidad
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto"></div></label>

<input readonly type="number" name="entry.2005063964" oninput="isvalid(this.id,'cedula')"  value="<?= $ci ?>" class="ss-q-short" id="entry_2005063964" dir="auto" aria-label="" aria-required="true" required="" step="any" title=""><span class="required-message" ></span><br/>
<div class="error-message" id="718776922_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-select"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_1613747071"><div class="ss-q-title">Talla de Franela
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto">Las tallas son masculinas. Si eres mujer y eres talla S o superior, debes escoger una talla menor.</div></label>

<input readonly type="text" class="ss-q-short" name="" value="<?= isset($_SESSION['entry_1613747071'])? htmlentities($_SESSION['entry_1613747071']) : '' ?>"/> <br/>
<select hidden name="entry.1613747071" id="entry_1613747071" aria-label="Talla de Franela Las tallas son masculinas. Si eres mujer y eres talla S o superior, debes escoger una talla menor. " aria-required="true" required=""><option value="<?= isset($_SESSION['entry_1613747071'])? htmlentities($_SESSION['entry_1613747071']) : '' ?>"></option>
<option value="4">4</option> <option value="6">6</option> <option value="8">8</option> <option value="10">10</option> <option value="12">12</option> <option value="14">14</option> <option value="16">16</option> <option value="S">S</option> <option value="M">M</option> <option value="L">L</option> <option value="XL">XL</option> <option value="XXL">XXL</option></select><br/>
<div class="required-message" hidden>Campo requerido</div></div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-checkbox"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_1334588207"><div class="ss-q-title">Sexo
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto"></div></label>

<input readonly type="text" class="ss-q-short" name="" value="<?= isset($_SESSION['entry_2128285595'])? htmlentities($_SESSION['entry_2128285595']) : '' ?>"/> <br/>
<ul class="ss-choices ss-choices-required" role="group" aria-label="Sexo  "><li class="ss-choice-item">
<label readonly><span class="ss-choice-item-control goog-inline-block"><input type="radio" name="entry.2128285595" data-onload="setrad(this.id,'<?= isset($_SESSION['entry_2128285595'])? htmlentities($_SESSION['entry_2128285595']) : ''?>')" value="M" id="group_2128285595_1" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">M</span>
</label></li> <li class="ss-choice-item">
<label readonly><span class="ss-choice-item-control goog-inline-block"><input type="radio" name="entry.2128285595" data-onload="setrad(this.id,'<?= isset($_SESSION['entry_2128285595'])? htmlentities($_SESSION['entry_2128285595']) : ''?>')" value="F" id="group_2128285595_2" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">F</span>
</label></li></ul><span class="required-message" ><?=  isset($_SESSION['error_entry_2128285595'])? htmlentities($_SESSION['error_entry_2128285595']) : ''  ?></span><br/>
<div class="error-message" id="1334588207_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div></div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-date"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_2101364991"><div class="ss-q-title">Fecha de Nacimiento
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto"></div></label>

<input readonly type="date" name="entry.2101364991" oninput="isvalid(this.id,'dob')" value="<?= isset($_SESSION['entry_2101364991'])? htmlentities($_SESSION['entry_2101364991']) : '' ?>" class="ss-q-date" dir="auto" id="entry_2101364991" aria-label="Fecha de Nacimiento  " aria-required="true" required=""><span class="required-message" ><?=  isset($_SESSION['error_entry_2101364991'])? htmlentities($_SESSION['error_entry_2101364991']) : ''  ?></span><br/>
<div class="required-message" hidden>Campo requerido</div></div></div></div> 

<!-- starting here .ss-form-gray -->
<div class="ss-form-gray">
<div class="ss-q-title">En caso de ser menor de edad:</div>
<div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="RepNombre"><div class="ss-q-title">Nombre y Apellido de representante acompañante en las Misiones
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto">Si usted es menor de edad porfavor coloque el nombre completo del representante quien lo acompañará. Si no aplica coloque n/a.</div></label>

<input readonly type="text" name="RepNombre" oninput="isvalid(this.id,'ful-name')" value="<?= isset($_SESSION['RepNombre'])? htmlentities($_SESSION['RepNombre']) : '' ?>" class="ss-q-short" id="RepNombre" dir="auto" aria-label="Nombre del Padre  " aria-required="true" required="" title=""><span class="required-message" ><?=  isset($_SESSION['RepNrror_nombre'])? htmlentities($_SESSION['RepNrror_nombre']) : ''  ?></span><br/>
<div class="error-message" id="1453397710_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="RepTel"><div class="ss-q-title">Teléfono de representante acompañante
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto">Ej: 0414-1234567.  Si no aplica coloque 7777-7777777.</div></label>

<input readonly type="text" name="RepTel" oninput="isvalid(this.id,'phone')" value="<?= isset($_SESSION['RepTel'])? htmlentities($_SESSION['RepTel']) : '' ?>" class="ss-q-short" id="RepTel" dir="auto" aria-label="Número de Celular  " aria-required="true" required="" title=""><span class="required-message" ><?=  isset($_SESSION['error_RepTel'])? htmlentities($_SESSION['error_RepTel']) : ''  ?></span><br/>
<div class="error-message" id="1354781914_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="RepParentesco"><div class="ss-q-title">Parentesco de representante acompañante
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto">Indíque cuál es el parentesco con el representante. Si no aplica coloque n/a.</div></label>

<input readonly type="text" name="RepParentesco" oninput="isvalid(this.id,'ful-name')" value="<?= isset($_SESSION['RepParentesco'])? htmlentities($_SESSION['RepParentesco']) : '' ?>" class="ss-q-short" id="RepParentesco" dir="auto" aria-label="Nombre de la Madre  " aria-required="true" required="" title=""><span class="required-message" ><?=  isset($_SESSION['error_RepParentesco'])? htmlentities($_SESSION['error_RepParentesco']) : ''  ?></span><br/>
<div class="error-message" ></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div>

</div>
<!-- ends here -->


<div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_1309052470"><div class="ss-q-title">Teléfono Fijo
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto">Ej: 0251-1234567</div></label>

<input readonly type="text" name="entry.1309052470" oninput="isvalid(this.id,'phone')" value="<?= isset($_SESSION['entry_1309052470'])? htmlentities($_SESSION['entry_1309052470']) : '' ?>" class="ss-q-short" id="entry_1309052470" dir="auto" aria-label="Teléfono Fijo Ej: 0251-1234567 " aria-required="true" required="" title=""><span class="required-message" ><?=  isset($_SESSION['error_entry_1309052470'])? htmlentities($_SESSION['error_entry_1309052470']) : ''  ?></span><br/>
<div class="error-message" id="160401832_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_1677385248"><div class="ss-q-title">Celular
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto">Ej: 0414-1234567</div></label>

<input readonly type="text" name="entry.1677385248" oninput="isvalid(this.id,'phone')" value="<?= isset($_SESSION['entry_1677385248'])? htmlentities($_SESSION['entry_1677385248']) : '' ?>" class="ss-q-short" id="entry_1677385248" dir="auto" aria-label="Celular Ej: 0414-1234567 " aria-required="true" required="" title=""><span class="required-message" ><?=  isset($_SESSION['error_entry_1677385248'])? htmlentities($_SESSION['error_entry_1677385248']) : ''  ?></span><br/>
<div class="error-message" id="1484970136_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_1522088102"><div class="ss-q-title">Correo Electrónico
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto">MUY IMPORTANTE: Coloca correctamente tu correo. Utiliza solo letras minusculas. En caso de no hacerlo, no te llegarán las informaciones de la preparación de misiones.</div></label>

<input type="email" name="entry.1522088102" oninput="isvalid(this.id,'email')" value="<?= isset($_SESSION['email'])? htmlentities($_SESSION['email']) : ''?>" class="ss-q-short" id="entry_1522088102" dir="auto" aria-label="Correo proporcionado" aria-required="true" required="" title="" readonly><br/>
<div class="error-message" id="610177552_errorMessage">Debe colocar una dirección de correo electrónico válida</div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-paragraph-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_514490750"><div class="ss-q-title">Dirección
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto">Colócala lo más detallada posible</div></label>

<textarea readonly name="entry.514490750" oninput="isvalid(this.id,'any')" rows="5" cols="0" class="ss-q-long" id="entry_514490750" dir="auto" aria-label="Dirección Colócala lo más detallada posible " aria-required="true" required=""><?= isset($_SESSION['entry_514490750'])? htmlentities($_SESSION['entry_514490750']) : '' ?></textarea><br/>
<div class="error-message" id="489063116_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_1338231974"><div class="ss-q-title">¿Dónde estudias?
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto">Si no estudias coloca n/a</div></label>

<input readonly type="text" name="entry.1338231974" oninput="isvalid(this.id,'any')" value="<?= isset($_SESSION['entry_1338231974'])? htmlentities($_SESSION['entry_1338231974']) : '' ?>" class="ss-q-short" id="entry_1338231974" dir="auto" aria-label="¿Dónde estudias? Si no estudias coloca n/a " aria-required="true" required="" title=""><span class="required-message" ><?=  isset($_SESSION['error_entry_1338231974'])? htmlentities($_SESSION['error_entry_1338231974']) : ''  ?></span><br/>
<div class="error-message" id="1612997785_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_446411834"><div class="ss-q-title">¿Qué estudias?
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto">Si no estudias coloca n/a</div></label>

<input readonly type="text" name="entry.446411834" oninput="isvalid(this.id,'any')" value="<?= isset($_SESSION['entry_446411834'])? htmlentities($_SESSION['entry_446411834']) : '' ?>" class="ss-q-short" id="entry_446411834" dir="auto" aria-label="¿Qué estudias? Si no estudias coloca n/a " aria-required="true" required="" title=""><span class="required-message" ><?=  isset($_SESSION['error_entry_446411834'])? htmlentities($_SESSION['error_entry_446411834']) : ''  ?></span><br/>
<div class="error-message" id="1675097615_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_1241364390"><div class="ss-q-title">¿Dónde trabajas?
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto">Si no trabajas coloca n/a</div></label>

<input readonly type="text" name="entry.1241364390" oninput="isvalid(this.id,'any')" value="<?= isset($_SESSION['entry_1241364390'])? htmlentities($_SESSION['entry_1241364390']) : '' ?>" class="ss-q-short" id="entry_1241364390" dir="auto" aria-label="¿Dónde trabajas? Si no trabajas coloca n/a " aria-required="true" required="" title=""><span class="required-message" ><?=  isset($_SESSION['error_entry_1241364390'])? htmlentities($_SESSION['error_entry_1241364390']) : ''  ?></span><br/>
<div class="error-message" id="891445109_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_80365004"><div class="ss-q-title">¿Profesión / Ocupación?
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto"></div></label>

<input readonly type="text" name="entry.80365004" oninput="isvalid(this.id,'any')" value="<?= isset($_SESSION['entry_80365004'])? htmlentities($_SESSION['entry_80365004']) : '' ?>" class="ss-q-short" id="entry_80365004" dir="auto" aria-label="¿Profesión / Ocupación?  " aria-required="true" required="" title=""><span class="required-message" ><?=  isset($_SESSION['error_entry_80365004'])? htmlentities($_SESSION['error_entry_80365004']) : ''  ?></span><br/>
<div class="error-message" id="1764119976_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-checkbox"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_763574105"><div class="ss-q-title">¿Cuáles Sacramentos tienes?
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto"></div></label>


<input id="sacramento" name="sacramento" readonly value="<?= isset($_SESSION['sacramento'])? htmlentities($_SESSION['sacramento']) : '' ?>" class="fillup-checkbox-input" required="" /><span class="required-message" ><?=  isset($_SESSION['error_sacramento'])? htmlentities($_SESSION['error_sacramento']) : ''  ?></span><br/>
<ul class="ss-choices ss-choices-required" role="group" aria-label="¿Cuáles Sacramentos tienes?  "><li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1294486902" data-onload="setval(this.id,'sacramento')" onclick="add(this.id,'sacramento')" value="Bautismo" id="group_1294486902_1" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">Bautismo</span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1294486902" data-onload="setval(this.id,'sacramento')" onclick="add(this.id,'sacramento')" value="Primera Comunión" id="group_1294486902_2" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">Primera Comunión</span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1294486902" data-onload="setval(this.id,'sacramento')" onclick="add(this.id,'sacramento')" value="Confirmación" id="group_1294486902_3" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">Confirmación</span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1294486902" data-onload="setval(this.id,'sacramento')" onclick="add(this.id,'sacramento')" value="Matrimonio" id="group_1294486902_4" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">Matrimonio</span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1294486902" data-onload="setval(this.id,'sacramento')" onclick="add(this.id,'sacramento')" value="Orden Sacerdotal" id="group_1294486902_5" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">Orden Sacerdotal</span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1294486902" data-onload="setval(this.id,'sacramento')" onclick="add(this.id,'sacramento')" value="Ninguno" id="group_1294486902_6" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">Ninguno</span>
</label></li></ul>
<div class="error-message" id="763574105_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div></div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_107097543"><div class="ss-q-title">¿Has participado en algún grupo, movimiento u orden de la Iglesia Católica?
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto">Si es así por favor compárterlo.  Si no aplica, coloca n/a</div></label>

<input readonly type="text" name="entry.107097543" oninput="isvalid(this.id,'any')" value="<?= isset($_SESSION['entry_107097543'])? htmlentities($_SESSION['entry_107097543']) : '' ?>" class="ss-q-short" id="entry_107097543" dir="auto" aria-label="" aria-required="true" required="" title=""><span class="required-message" ><?=  isset($_SESSION['error_entry_107097543'])? htmlentities($_SESSION['error_entry_107097543']) : ''  ?></span><br/>
<div class="error-message" id="646731620_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="noCatolica"><div class="ss-q-title">¿Has participado en algún grupo, movimiento de otra iglesia o religión?
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto">Si es así por favor compárterlo. Si no aplica, coloca n/a</div></label>

<input readonly type="text" name="noCatolica" oninput="isvalid(this.id,'any')" value="<?= isset($_SESSION['noCatolica'])? htmlentities($_SESSION['noCatolica']) : '' ?>" class="ss-q-short" id="noCatolica" dir="auto" aria-label="" aria-required="true" required="" title=""><span class="required-message" ><?=  isset($_SESSION['error_noCatolica'])? htmlentities($_SESSION['error_noCatolica']) : ''  ?></span><br/>
<div class="error-message" id="646731620_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_1204949772"><div class="ss-q-title">¿A través de quién llegaste a PdA?
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto">Coloca el nombre de la persona que te trajo o te invitó al grupo</div></label>

<input readonly type="text" name="entry.1204949772" oninput="isvalid(this.id,'ful-name')" value="<?= isset($_SESSION['entry_1204949772'])? htmlentities($_SESSION['entry_1204949772']) : '' ?>" class="ss-q-short" id="entry_1204949772" dir="auto" aria-label="¿A través de quién llegaste a PdA? Coloca el nombre de la persona que te trajo o te invitó al grupo " aria-required="true" required="" title=""><span class="required-message" ><?=  isset($_SESSION['error_entry_1204949772'])? htmlentities($_SESSION['error_entry_1204949772']) : ''  ?></span><br/>
<div class="error-message" id="491769639_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-checkbox"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_1433690086"><div class="ss-q-title">¿A qué Misiones de PdA has asistido?
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto"></div></label>

<input id="misiones" name="misiones" readonly value="<?= isset($_SESSION['misiones'])? htmlentities($_SESSION['misiones']) : '' ?>" class="fillup-checkbox-input" required="" /><span class="required-message" ><?=  isset($_SESSION['error_misiones'])? htmlentities($_SESSION['error_misiones']) : ''  ?></span><br/>
<ul class="ss-choices ss-choices-required" role="group" aria-label="¿A qué Misiones de PdA has asistido?  "><li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.586164515" data-onload="setval(this.id,'misiones')" onclick="add(this.id,'misiones')" value="2004" id="group_586164515_1" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">2004 Carache</span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.586164515" data-onload="setval(this.id,'misiones')" onclick="add(this.id,'misiones')" value="2005" id="group_586164515_2" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">2005 Boconó</span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.586164515" data-onload="setval(this.id,'misiones')" onclick="add(this.id,'misiones')" value="2006" id="group_586164515_3" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">2006 Timotes</span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.586164515" data-onload="setval(this.id,'misiones')" onclick="add(this.id,'misiones')" value="2007" id="group_586164515_4" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">2007 Chachopo - Mucuchíes - El Salado</span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.586164515" data-onload="setval(this.id,'misiones')" onclick="add(this.id,'misiones')" value="2008" id="group_586164515_5" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">2008 La Azulita</span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.586164515" data-onload="setval(this.id,'misiones')" onclick="add(this.id,'misiones')" value="2009" id="group_586164515_6" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">2009 Santa Cruz de Mora</span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.586164515" data-onload="setval(this.id,'misiones')" onclick="add(this.id,'misiones')" value="2010" id="group_586164515_7" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">2010 Lagunillas</span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.586164515" data-onload="setval(this.id,'misiones')" onclick="add(this.id,'misiones')" value="2011" id="group_586164515_8" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">2011 Carora - Barbacoas</span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.586164515" data-onload="setval(this.id,'misiones')" onclick="add(this.id,'misiones')" value="2012" id="group_586164515_9" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">2012 Siquisique</span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.586164515" data-onload="setval(this.id,'misiones')" onclick="add(this.id,'misiones')" value="2013" id="group_586164515_10" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">2013 Moroturo</span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.586164515" data-onload="setval(this.id,'misiones')" onclick="add(this.id,'misiones')" value="2014" id="group_586164515_11" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">2014 Siquisique</span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.586164515" data-onload="setval(this.id,'misiones')" onclick="add(this.id,'misiones')" value="2015" id="group_586164515_12" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">2015 Río Tocuyo - Curarigua - Atarigua</span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.586164515" data-onload="setval(this.id,'misiones')" onclick="add(this.id,'misiones')" value="2016" id="group_586164515_13" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">2016 </span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.586164515" data-onload="setval(this.id,'misiones')" onclick="add(this.id,'misiones')" value="2017" id="group_586164515_14" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">2017 </span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.586164515" data-onload="setval(this.id,'misiones')" onclick="add(this.id,'misiones')" value="2018" id="group_586164515_15" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">2018 </span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.586164515" data-onload="setval(this.id,'misiones')" onclick="add(this.id,'misiones')" value="Ninguna (no he misionado con PDA)" id="group_586164515_13" role="checkbox" class="ss-q-checkbox" aria-required="true"></span>
<span class="ss-choice-label">Ninguna (no he misionado con PDA)</span>
</label></li></ul>
<div class="error-message" id="1433690086_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div></div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_992502456"><div class="ss-q-title">Si tienes novio(a)/esposo(a) en PDA por favor colóca su nombre y apellido
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto">Si no tienes, coloca n/a</div></label>

<input readonly type="text" name="entry.992502456" oninput="isvalid(this.id,'ful-name')" value="<?= isset($_SESSION['entry_992502456'])? htmlentities($_SESSION['entry_992502456']) : '' ?>" class="ss-q-short" id="entry_992502456" dir="auto" aria-label="Si tienes novio(a)/esposo(a) en PDA por favor coloca su nombre y apellido Si no tienes, coloca n/a " aria-required="true" required="" title=""><span class="required-message" ><?=  isset($_SESSION['error_entry_992502456'])? htmlentities($_SESSION['error_entry_992502456']) : ''  ?></span><br/>
<div class="error-message" id="556349049_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item  ss-checkbox"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_1676998144"><div class="ss-q-title">¿Cual de éstas habilidades posees?
</div>
<div class="ss-q-help ss-secondary-text" dir="auto"></div></label>

<input id="habili" name="habilidad" readonly value="<?= isset($_SESSION['habilidad'])? htmlentities($_SESSION['habilidad']) : '' ?>" class="fillup-checkbox-input" required="" /><span class="required-message" ><?=  isset($_SESSION['error_habilidad'])? htmlentities($_SESSION['error_habilidad']) : ''  ?></span><br/>
<ul class="ss-choices" role="group" aria-label="¿Cual de éstas habilidades posees?  "><li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1486206645" data-onload="setval(this.id,'habili')" onclick="add(this.id,'habili')" value="Recrear" id="group_1486206645_1" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">Recrear</span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1486206645" data-onload="setval(this.id,'habili')" onclick="add(this.id,'habili')" value="Orar en público" id="group_1486206645_2" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">Orar en público</span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1486206645" data-onload="setval(this.id,'habili')" onclick="add(this.id,'habili')" value="Tocar un instrumento o cantar" id="group_1486206645_3" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">Tocar un instrumento/cantar</span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1486206645" data-onload="setval(this.id,'habili')" onclick="add(this.id,'habili')" value="Hacer manualidades o papelería" id="group_1486206645_4" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">Hacer manualidades (papelería)</span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1486206645" data-onload="setval(this.id,'habili')" onclick="add(this.id,'habili')" value="Cocinar" id="group_1486206645_5" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">Cocinar</span>
</label></li> <li class="ss-choice-item">
<label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1486206645" data-onload="setval(this.id,'habili')" onclick="add(this.id,'habili')" value="Otra" id="group_1486206645_6" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">Otra</span>
</label></li></ul>
<div class="error-message" id="1676998144_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div></div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item  ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_1252199433"><div class="ss-q-title">¿Qué otra?
</div>
<div class="ss-q-help ss-secondary-text" dir="auto"></div></label>

<input readonly type="text" name="entry.1252199433" oninput="isvalid(this.id,'any')" value="<?= isset($_SESSION['entry_1252199433'])? htmlentities($_SESSION['entry_1252199433']) : '' ?>" class="ss-q-short valid" id="entry_1252199433" required="" dir="auto" aria-label="¿Qué otra?  " title=""><span class="required-message" ><?=  isset($_SESSION['error_entry_1252199433'])? htmlentities($_SESSION['error_entry_1252199433']) : ''  ?></span><br/>
<div class="error-message" id="560461902_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item  ss-select"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_507951984"><div class="ss-q-title">Tipo de sangre
</div>
<div class="ss-q-help ss-secondary-text" dir="auto">Si no lo sabes, déjalo en blanco</div></label>

<input readonly type="text" name="" class="ss-q-short" value="<?= isset($_SESSION['entry_507951984'])? htmlentities($_SESSION['entry_507951984']) : '' ?>"/><br/>
<select hidden name="entry.507951984" id="entry_507951984" aria-label="Tipo de sangre Si no lo sabes, déjalo en blanco " class="valid"><option value=""></option>
<option value="A+">A+</option> <option value="B+">B+</option> <option value="AB+">AB+</option> <option value="O+">O+</option> <option value="A-">A-</option> <option value="B-">B-</option> <option value="AB-">AB-</option> <option value="O-">O-</option></select>
<div class="required-message" hidden>Campo requerido</div></div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item  ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_1907784252"><div class="ss-q-title">¿Padeces de alguna enfermedad o condición especial? ¿Cuál(es)?
</div>
<div class="ss-q-help ss-secondary-text" dir="auto"></div></label>

<input readonly type="text" name="entry.1907784252" oninput="isvalid(this.id,'any')" value="<?= isset($_SESSION['entry_1907784252'])? htmlentities($_SESSION['entry_1907784252']) : '' ?>" class="ss-q-short" required="" id="entry_1907784252" dir="auto" aria-label="¿Padeces de alguna enfermedad o condición especial? ¿Cuál(es)?  " title=""><span class="required-message" ><?=  isset($_SESSION['error_entry_1907784252'])? htmlentities($_SESSION['error_entry_1907784252']) : ''  ?></span><br/>
<div class="error-message" id="1883668650_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item  ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_2069387399"><div class="ss-q-title">¿Tomas algún medicamento?  ¿Cuál(es)?
</div>
<div class="ss-q-help ss-secondary-text" dir="auto"></div></label>

<input readonly type="text" name="entry.2069387399" oninput="isvalid(this.id,'any')" value="<?= isset($_SESSION['entry_2069387399'])? htmlentities($_SESSION['entry_2069387399']) : '' ?>" class="ss-q-short" required="" id="entry_2069387399" dir="auto" aria-label="¿Tomas algún medicamento?  ¿Cuál(es)?  " title=""><span class="required-message" ><?=  isset($_SESSION['error_entry_2069387399'])? htmlentities($_SESSION['error_entry_2069387399']) : ''  ?></span><br/>
<div class="error-message" id="699528707_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item  ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_1262205387"><div class="ss-q-title">¿Eres alérgico(a)? Colócalo aquí
</div>
<div class="ss-q-help ss-secondary-text" dir="auto">Ej: Medicamentos, comidas, picadas de insectos, materiales, etc.</div></label>

<input readonly type="text" name="entry.1262205387" oninput="isvalid(this.id,'any')" value="<?= isset($_SESSION['entry_1262205387'])? htmlentities($_SESSION['entry_1262205387']) : '' ?>" class="ss-q-short" required="" id="entry_1262205387" dir="auto" aria-label="¿Eres alérgico(a)? Colócalo aquí Ej: Medicamentos, comidas, picadas de insectos, materiales, etc. " title=""><span class="required-message" ><?=  isset($_SESSION['error_entry_1262205387'])? htmlentities($_SESSION['error_entry_1262205387']) : ''  ?></span><br/>
<div class="error-message" id="1897587821_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_704382283"><div class="ss-q-title">Nombre del Padre
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto"></div></label>

<input readonly type="text" name="entry.704382283" oninput="isvalid(this.id,'ful-name')" value="<?= isset($_SESSION['entry_704382283'])? htmlentities($_SESSION['entry_704382283']) : '' ?>" class="ss-q-short" id="entry_704382283" dir="auto" aria-label="Nombre del Padre  " aria-required="true" required="" title=""><span class="required-message" ><?=  isset($_SESSION['error_entry_704382283'])? htmlentities($_SESSION['error_entry_704382283']) : ''  ?></span><br/>
<div class="error-message" id="1453397710_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_147384584"><div class="ss-q-title">Número de Celular
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto">Ej: 0414-1234567</div></label>

<input readonly type="text" name="entry.147384584" oninput="isvalid(this.id,'phone')" value="<?= isset($_SESSION['entry_147384584'])? htmlentities($_SESSION['entry_147384584']) : '' ?>" class="ss-q-short" id="entry_147384584" dir="auto" aria-label="Número de Celular  " aria-required="true" required="" title=""><span class="required-message" ><?=  isset($_SESSION['error_entry_147384584'])? htmlentities($_SESSION['error_entry_147384584']) : ''  ?></span><br/>
<div class="error-message" id="1354781914_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_1941103996"><div class="ss-q-title">Nombre de la Madre
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto"></div></label>

<input readonly type="text" name="entry.1941103996" oninput="isvalid(this.id,'ful-name')" value="<?= isset($_SESSION['entry_1941103996'])? htmlentities($_SESSION['entry_1941103996']) : '' ?>" class="ss-q-short" id="entry_1941103996" dir="auto" aria-label="Nombre de la Madre  " aria-required="true" required="" title=""><span class="required-message" ><?=  isset($_SESSION['error_entry_1941103996'])? htmlentities($_SESSION['error_entry_1941103996']) : ''  ?></span><br/>
<div class="error-message" id="1613962121_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_1126146889"><div class="ss-q-title">Número de Celular
<label for="itemView.getDomIdToLabel()" aria-label="(Required field)"></label>
<span class="ss-required-asterisk" aria-hidden="true">*</span></div>
<div class="ss-q-help ss-secondary-text" dir="auto">Ej: 0414-1234567</div></label>

<input readonly type="text" name="entry.1126146889" oninput="isvalid(this.id,'phone')" value="<?= isset($_SESSION['entry_1126146889'])? htmlentities($_SESSION['entry_1126146889']) : '' ?>" class="ss-q-short" id="entry_1126146889" dir="auto" aria-label="Número de Celular  " aria-required="true" required="" title=""><span class="required-message" ><?=  isset($_SESSION['error_entry_1126146889'])? htmlentities($_SESSION['error_entry_1126146889']) : ''  ?></span><br/>
<div class="error-message" id="1516457543_errorMessage"></div>
<div class="required-message" hidden>Campo requerido</div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item  ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_1560839713"><div class="ss-q-title">Disponibilidad de vehículo
</div>
<div class="ss-q-help ss-secondary-text" dir="auto">Si tienes carro/camioneta a tu disposición con el que puedas colaborar en las diversas actividades de PDA, compártelo aquí:</div></label>

<input readonly type="text" name="entry.1560839713" oninput="isvalid(this.id,'')" value="<?= isset($_SESSION['entry_1560839713'])? htmlentities($_SESSION['entry_1560839713']) : '' ?>" class="ss-q-short" id="entry_1560839713" dir="auto" aria-label="Disponibilidad de vehículo Si tienes carro/camioneta a tu disposición con el que puedas colaborar en las diversas actividades de PDA, compártelo aquí: " title=""><span class="required-message" ><?=  isset($_SESSION['error_entry_1560839713'])? htmlentities($_SESSION['error_entry_1560839713']) : ''  ?></span><br/>
<div class="error-message" id="1510483744_errorMessage"></div>
<div class="required-message"></div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="auto" class="ss-item  ss-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="emigrar"><div class="ss-q-title">¿Tienes planes de irte del pais este año?
</div>
<div class="ss-q-help ss-secondary-text" dir="auto">Compártelo aquí:</div></label>

<input readonly type="text" name="emigrar" oninput="isvalid(this.id,'')" value="<?= isset($_SESSION['emigrar'])? htmlentities($_SESSION['emigrar']) : '' ?>" class="ss-q-short" id="emigrar" dir="auto" aria-label="Disponibilidad de vehículo Si tienes carro/camioneta a tu disposición con el que puedas colaborar en las diversas actividades de PDA, compártelo aquí: " title=""><span class="required-message" ><?=  isset($_SESSION['error_emigrar'])? htmlentities($_SESSION['error_emigrar']) : ''  ?></span><br/>
<div class="error-message" id="1510483745_errorMessage"></div>
<div class="required-message"></div>
</div></div></div></div></ol>
</form>
</div></div></div></div></div>
</main>

<div align="center" class="mb-4">
<a href="session.php" class="btn btn-outline-info col-md-2" tabindex="-1" role="button" aria-disabled="true">Volver</a>
</div>

<?php include("../inc/footer.php") ?>
<script>
    $('[data-onload]').each(function(){
        eval($(this).data('onload'));
    });
    </script>
</body> 
</html>
