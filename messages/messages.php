<?php


function registerMailMsg($name,$email,$token){
    $body = "<p>$name,</p>
    <p>Te damos la bienvenida a Proyecto de Amor. Tu registro ha sido un éxito. Para continuar con la configuración de tu cuenta requerimos que la actives haciendo click en el siguiente botón:</p>
    <div align='center'>
    <a href='https://pda.org.ve/miembro/login/confirm.php?email=$email&action=0&token=$token'><button style='background: #2196f3;border: none; color: white;padding: 1rem;'>Activa tu cuenta</button></a>
    <br/><br/>
    <p>Si tienes dificultad con el botón por favor copia este enlace y pégalo en la barra de dirección de tu navegador de preferencia:  <a href='https://pda.org.ve/miembro/login/confirm.php?email=$email&action=0&token=$token'>https://pda.org.ve/miembro/login/confirm.php?email=$email&action=0&token=$token</a></p>
    <br/>
    </div>
    <p>Dios te bendiga.</p>
    <br/>
        --------------------------------------
    <p>Este correo es automático, Por favor ¡no respondas a esta dirección!.<br/>Si deseas comunicarte con nosotros contáctanos a ".'<a href="mailto:proyectodeamor@gmail.com">proyectodeamor@gmail.com</a>'."</p>";
    return $body;
}

function resetPassMailMsg($email,$token){
    $body = "<p>Hola,</p>
    <p>Has recibido este correo porque has solicitado restablecer la contraseña.  Para proceder con tu restauración de contraseña haz click en el siguiente botón:</p>
    <div align='center'>
    <a href='https://pda.org.ve/miembro/login/confirm.php?email=$email&action=1&token=$token'><button style='background: orange;border: none; color: white;padding: 1rem;'>Establece una nueva contraseña</button></a>
    <br/><br/>
    <p>Si tienes dificultad con el botón por favor copia este enlace y pégalo en la barra de dirección de tu navegador de preferencia:  <a href='https://pda.org.ve/miembro/login/confirm.php?email=$email&action=1&token=$token'>https://pda.org.ve/miembro/login/confirm.php?email=$email&action=1&token=$token</a></p>
    <br/>
    <p>Si no solicitaste restablecer la contraseña y estás recibiendo éste correo, inicia sesión con tus credenciales de costumbre y listo.</p>
    </div>
    <p>Dios te bendiga.</p>
    <br/>
        --------------------------------------
    <p>Este correo es automático, Por favor ¡no respondas a esta dirección!.<br/>Si deseas comunicarte con nosotros contáctanos a ".'<a href="mailto:proyectodeamor@gmail.com">proyectodeamor@gmail.com</a>'."</p>";
    return $body;
}

function sessionMsg($needTo){
    if($needTo==true){
        $msg = '<p style="color:gray;">Tu cuenta ya esta activa. Por favor ingresa tus datos personales.  Éstos serán utilizados para el proceso de inscripción de misiones.</p>';
    }
    else{
        $msg = '<p style="color:gray;">Si deseas revisar los datos que ingresaste, haz click en VER, si por lo contrario deseas hacer algun ajuste, por favor haz click en MODIFICAR.</p>';
    }
    return $msg;
}

function resetPassMsg($emailSent){
    if($emailSent){
        $msg = '<p style="color: green; min-height: 30vh;">¡Te hemos enviado un correo de recuperación! Ahora busca en tu bandeja de entrada el correo de activación de tu cuenta, recuerda revisar la carpeta de spam.</p>';
    }else{
        $msg = '<p style="color: #f44336; min-height: 30vh;">¡Oops! Algo extraño ocurrió.  No pudimos creat tu cuenta.  Reporta este incidente al administrador del sitio web.  Porfavor Intenta de nuevo.  Gracias por tu paciencia.</p>';
    }
    return $msg;
}

function estadoDeSolvencia($estaSolvente,$evento){
      $estado = "";
    if(!$estaSolvente){
      $estado = '<p style="color:#f44336;">1.&#9;<span><i style="color:red;" class="col-1 fas fa-times"></i></span> El usuario presenta deuda con el grupo. Para asistir a: <strong>'.$evento.'</strong> es necesario estar al día, por favor contacta a proyectodeamor@gmail.com para gestionar el pago.</p>';
    }
    return $estado;
}

function disclaimerMsg(){
    $mensaje='<ul>
    <li>Proyecto de Amor se reserva el derecho de admitir o no la inscripción de los misioneros luego de verificados sus datos.</li>
    <li>No se permite la inscripción de menores de edad de nuevo ingreso que vayan solos. Sólo podrán inscribirse si asisten a la preparación de misiones y a las misiones junto con su representante legal, quien también debe inscribirse y cumplir con todos los requisitos de asistencia y formación. En caso de no cumplir con este requisito, la inscripción de estos menores de edad será descartada.</li>
    <li>La inscripción en esta página no es garantia de asistencia a las Misiones. Para esto el misionero debe cumplir con una serie de requisitos de formación, asistencia, participación en actividades profondo y cualquier otro que disponga la coordinación de PDA.</li>
    <li>El canal para las informaciones oficiales referentes a las misiones es el correo electrónico, por lo que el misionero debe estar atento a revisar constantemente este medio.</li>
    <li>Los menores de edad de nuevo ingreso (inscritos) deben formalizar su inscripción presentando la cédula de identidad o partida de nacimiento en compañía de su representante legal (inscrito también). Esto debe realizarse en la 1ra y 2da formación. Quienes no cumplan este requisito les será anulada su inscripción.</li>
</ul>';
    return $mensaje;
}


function reporte_para_cambiar_cedula($dataToUpdate){
    $reporte = '';
    if($dataToUpdate != ''){
        $reporte.= '<h4 class="col-12"> Resultados</h4>';

    if ($dataToUpdate['exist_email'] !== '&#63;' && $dataToUpdate['exist_cedulaNew'] !== '&#63;' && $dataToUpdate['exist_usr_id'] !== '&#63;' && $dataToUpdate['exist_cedulaAct'] !== '&#63;' && $dataToUpdate['exist_deuda'] !== '&#63;' && $dataToUpdate['exist_misionero'] !== '&#63;' && $dataToUpdate['exist_mensage'] !== '&#63;'){
        $reporte.= '<span><i style="color:green;" class="col-1 fas fa-check"></i> </span>';
    $reporte.= '<div class="col-11 my-2">';
    $reporte.='
    <span class="small"> Comentarios: <strong>' .$dataToUpdate['exist_mensage'].'</strong></span><br/>
    <span class="small"> Correo electronico en registro: <strong>'.$dataToUpdate['exist_email'].'</strong></span><br/>
    <span class="small"> Cédula Deseada: <strong> V-' .$dataToUpdate['exist_cedulaNew'].'</strong></span><br/>
    <span class="small"> Cédula Actual: <strong> V-' .$dataToUpdate['exist_cedulaAct'].'</strong></span><br/>

    <span class="small"> Presenta Deuda: <strong>' .(($dataToUpdate['exist_deuda']==0)?' no':'si').'</strong></span><br/>
    <span class="small"> Tipo de misionero: <strong>' .(($dataToUpdate['exist_misionero']==0)?' nuevo':' antiguo').'</strong></span><br/>
    </div>';

        if($dataToUpdate['exist_deuda']==0){
    $reporte.= '<div class="col-md-12">
    <p>¿Desea proceder con el cambio de número de cédula?</p>
        <form action="" method="POST" class="row m-1 justify-content-center">
        <div class="ss-cedula-number">
<input type="number" name="id" value="'.$dataToUpdate['exist_usr_id'].'" hidden="">
<input type="text" name="misionero" value="'.$dataToUpdate['exist_misionero'].'" hidden="">
<input type="text" name="email" value="'.$dataToUpdate['exist_email'].'" hidden="">
<input type="text" name="ci" value="'.$dataToUpdate['exist_cedulaNew'].'" hidden="">
</div>
        <div>
        <button type="submit" name="update_s_id" class="btn btn-danger btn-lg"><i class="far fa-save"></i> Guardar</button>
        <a href="" class="btn btn-secondary btn"><i class="fas fa-times"></i> Cancelar</a>
        </div>
        </form>
        </div>';

        }else{
            $reporte.= '<span style="color:red;">No se puede hacer ningun cambio. Revise en comentarios</span>';
        }

    }else{
    $reporte.= '<span><i style="color:red;" class="col-1 fas fa-times"></i> Se encontraron los siguiente errores</span>';
    $reporte.= '<div class="col-sm-8 my-2">';
    $reporte.= '<span class="small"> Comentarios: <strong>' .$dataToUpdate['exist_mensage'].'</strong></span><br/>';
    $reporte.= '<span class="small"> Cédula Nueva: <strong> V-' .$dataToUpdate['exist_cedulaNew'].'</strong></span><br/>';
    $reporte.=  '<span class="small"> Correo electronico en registro: <strong>'.$dataToUpdate['exist_email'].'</strong></span><br/>';
    $reporte.= '<span class="small"> Cédula a modificar: <strong> V-' .$dataToUpdate['exist_cedulaAct'].'</strong></span><br/>';
    $reporte.= '<span class="small"> Presenta Deuda: <strong>' .$dataToUpdate['exist_deuda'].'</strong></span><br/>';
    $reporte.= '<span class="small"> Tipo de misionero: <strong>' .$dataToUpdate['exist_misionero'].'</strong></span><br/>';
    $reporte.= '</div>';
    }

    }

    return $reporte;
}

function constructHTMLReporte($reporte){

  foreach ($reporte as $key) {
    echo '<tr>';
    echo '<td>'.$key["name"].'</td>';
    echo '<td>'. ($key["nuevos"] + $key["viejos"]) .'</td>';
    echo '<td>'.$key["nuevos"].'</td>';
    echo '<td>'.$key["viejos"].'</td>';
    echo '</tr>';
  }

  // echo '<div class="col-md-10 text-centered" style="color:#FF8903;">';
  // echo 'Reporte Inscripción: (total: <strong>';
  // echo (intval($m_inscritosViejos)+intval($m_inscritosNuevos)).'</strong>)<br/>';
  // echo '<span class="small">(Para refrescar los números dale a sesión)</span>';
  // echo '<p>';
  // echo 'Misioneros viejos (inscritos): <strong>' . $m_inscritosViejos.'</strong>, ';
  // echo 'Misioneros nuevos (inscritos): <strong>' . $m_inscritosNuevos.'</strong>';
  // echo'</p>';
  // echo '</div>';

}


function reporteDeInscripcionMsg($priv){
    if($priv === 5){
      echo '<div class="container">';
          echo '<div class="row justify-content-center">';
              echo '<div class="col-md-12" align="center" style="color:#FF8903;">';
                      echo '<table class="report-event-table">';
                          echo '<tr>';
                            echo '<th>Evento</th>';
                            echo '<th>Total</th>';
                            echo '<th>Misioneros Nuevos</th>';
                            echo '<th>Misioneros Viejos</th>';
                          echo '</tr>';

                            reporteInscritosAdmin();

                      echo '</table>';
                      echo '<span class="small text-left">[inf sólo para administradores] (Para refrescar los números dale a sesión)</span>';
              echo '</div>';
          echo '</div>';
      echo '</div>';
    }
}
