

<div class="container">
    <div class="row content-justified font-italic">
        <div class="col-12">
               <p>Por favor lea la siguiente información y si está de acuerdo habla click en "Acepto" para formalizar su inscripción en las Misiones 2019:</p>

               <div>
                 <br/>
                 <p>APORTE A PAGAR:<br/>
                  Debido a la constante devaluación y a que es prácticamente imposible fijar un precio que se mantenga por los próximos meses, se ha decidido fijar el monto a pagar en dólares, pudiendo ser pagados en dólares o bolívares dependiendo de la tasa del día. El monto a pagar es el siguiente:</p>

                  <p class="font-weigh-bold">Aporte Básico:<br/>
                  12 $ (Para todos los misioneros a partir de 12 años)</p>

                  <p>Franela:<br/>
                  6 $ (Obligatorio para nuevo ingreso, opcional para el resto)<br/></p>

                  <p>Descuento por pronto pago sobre el aporte básico:<br/>
                  -2 $  (Si paga antes del 16-02)<br/>
                  -1 $  (Si paga antes del 16-03)<br/>
                  (No aplica para el pago de la franela)</p>

                  <p>Descuento por edad:<br/>
                  -50% de Descuento del Aporte Básico y Franelas.
                  (Aplica para Niños de 11 años o menos.)</p>




                  <p>CONDICIONES PARA MENORES DE EDAD:</p>
                  <ul>
                    <li>No se permite la inscripción de menores de edad de nuevo ingreso que vayan solos. Sólo podrán inscribirse si asisten a la preparación de misiones y a las misiones junto con su representante legal, quien también debe inscribirse y cumplir con todos los requisitos de asistencia y formación. En caso de no cumplir con este requisito, la inscripción de estos menores de edad será descartada.</li>
                     <li>Los menores de edad de nuevo ingreso (inscritos) deben formalizar su inscripción presentando la cédula de identidad o partida de nacimiento en compañía de su representante legal (inscrito también). Esto debe realizarse en la 1ra y 2da formación. Quienes no cumplan este requisito les será anulada su inscripción.</li>
                  </ul>

                  <p>CONDICIONES GENERALES:</p>
                  <ul>
                     <li>Proyecto de Amor se reserva el derecho de admitir o no la inscripción de los misioneros luego de verificados sus datos.</li>

                     <li>La inscripción en esta página no es garantía de asistencia a las Misiones. Para esto el misionero debe cumplir con una serie de requisitos de formación, asistencia, participación en actividades profondo y cualquier otro que disponga la coordinación de PDA.</li>

                     <li>El canal para las informaciones oficiales referentes a las misiones es el correo electrónico, por lo que el misionero debe estar atento a revisar constantemente este medio.</li>
                  </ul>
              </div>
        </div>
        <div class="col-12" align="center">
            <form method="POST" action="">
            <input type="text" value=<?=$id?> hidden>
            <input type="submit" name="submit" value="Acepto" id="ss-submit" class="btn btn-primary"/>
            <input type="submit" name="cancel" value="No Acepto" id="ss-cancel" class="btn btn-secondary"/>
            </form>
        </div>
    </div>
</div>
