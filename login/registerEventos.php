<?php
// die; // get rid of this when time is right
require_once "repeat.php";
require_once "../inc/db_mngmt.php";
require_once "../messages/messages.php";
require_once "../model/data.php";

if(!isset($_SESSION))
    session_start();

if(!isset($_SESSION['email']) || !isset($_SESSION['pass']))
    redirect(0,"login.php");

if(isset($_POST['cancel']) || !isset($_GET['evento'])){
        $_SESSION['msg'] = '';
        redirect(0,"session.php");
        exit;
    }


    $email = $_SESSION["email"];
    $ci  = $_SESSION['cedula'];
    $id = $_SESSION["usr_id"];
    $hash = $_SESSION["pass"];
    $msg = $_SESSION["msg"];
    $loggedInTime = $_SESSION["loggedIn"];

    $misionero = $_SESSION["tipo_misionero"];
    $needtoFill = $_SESSION['needToFill'];
    $solvente = $_SESSION['solvente'];

    $msg2 ='';

    if($needtoFill){
        $_SESSION['msg'] = '<div class="popup-message">'.'<p style="color:red; font-weight:bold;">¡Error! Usted no puede inscribirse hasta que haya registrado sus datos en el sistema.  Por favor ingrese sus datos haciendo click en el botón que dice Ingresar Datos.</p>'.'</div>';
        redirect(0,"session.php");
        exit;
    }

    // coger el valor del Evento, limpiarlo tambien
    $keyEvento = htmlentities($_GET['evento']);
    // echo $keyEvento;
    if( !array_key_exists($keyEvento, $eventosPDA) ){
      $_SESSION['msg'] = '<div class="popup-message">'.'<p style="color:red; font-weight:bold;">Evento inválido!</p></div>';
      redirect(0,"session.php");
    }

    // echo $keyEvento. '<pre>';
    // print_r($eventosPDA[$keyEvento]);
    // echo '</pre>';

    if(!$solvente){
      $_SESSION['msg'] = '<div class="popup-message">'. estadoDeSolvencia($solvente, $eventosPDA['minimisiones19']['name']) .'</div>';
      redirect(0,"session.php");
      exit;
    }

    // 0 means not open yet
    if($_SESSION['inscripcionAbierta']['ready'] == 0){
      $_SESSION['msg'] = '<div class="popup-message">'.$_SESSION['inscripcionAbierta']['msg'].'</div>';
      redirect(0,"session.php");
      exit;
    }


    if(isset($_POST['submit'])){
      // test for user input InvalidArgumentException
      require "server.php";
      $_SESSION['in_fechaPago'] = $mysqli->escape_string($_POST['in_fechaPago']);
      $_SESSION['in_banco'] = $mysqli->escape_string($_POST['in_banco']);
      $_SESSION['in_cedula'] = $mysqli->escape_string($_POST['in_cedula']);
      $_SESSION['in_referencia'] = $mysqli->escape_string($_POST['in_referencia']);
      $_SESSION['in_moneda'] = $mysqli->escape_string($_POST['in_moneda']);
      $_SESSION['in_monto'] = doubleval(htmlentities($_POST['in_monto']));
      $_SESSION['in_kilo'] = $mysqli->escape_string($_POST['in_kilo']);
      $mysqli->close();


      if( !empty($_POST['in_fechaPago']) && !empty($_POST['in_banco']) && !empty($_POST['in_cedula']) && !empty($_POST['in_referencia']) &&
      !empty($_POST['in_moneda']) && !empty($_POST['in_monto']) && !empty($_POST['in_kilo']))
      {
          if($_SESSION['in_monto'] != 0.0){
            $allGood = true;
            // echo 'allgood';
          }else{
            $allGood = false;
            // echo 'all NO good';
              $msg = '<p style="color:red;">Coloque sólo números</p>';
          }
      }else{
          $allGood = false;
          // echo 'all NO good';
      }

      if($allGood === true){
        require "server.php";

        $in_fechaPago = $_SESSION['in_fechaPago'];
        $in_banco = $_SESSION['in_banco'];
        $in_cedula = $_SESSION['in_cedula'];
        $in_referencia = $_SESSION['in_referencia'];
        $in_moneda = $_SESSION['in_moneda'];
        $in_monto = $_SESSION['in_monto'];
        $in_kilo = $_SESSION['in_kilo'];
        $evento_id = $eventosPDA[$keyEvento]['id'];
        // echo '<pre>';
        // var_dump($_SESSION);
        // echo '</pre>';
        // echo "go on and add the row in eventoPago ( update the 'asistirMision' to 1");
        //$sql = "UPDATE usuario SET asistir_mision=1 WHERE usr_id='$id';";
        $sql = "INSERT IGNORE INTO eventoPago (misioneroID, eventoID, fechaPago, nombreBanco, cedulaTitular, nroReferencia, tipoMoneda, monto, kilo)
         VALUES ('$id','$evento_id','$in_fechaPago', '$in_banco', '$in_cedula', '$in_referencia', '$in_moneda', '$in_monto', '$in_kilo'); ";
        $result = $mysqli->query($sql) or die($mysqli->error);
        // var_dump($result);
        // die;


        //TODO: enviar correo reportandole a la gente lo que coloco.





        $_SESSION['msg'] = '<div class="popup-message">'.$_SESSION['inscripcionAbierta']['msg'].'</div>';
        $mysqli->close();
        redirect(0,"session.php");
        exit;
      }
      else{
        $msg = '<p style="color:red;">Llene todo el formulario</p>';


        // errors
      }

    }


    if(isset($_POST['kilo-submit'])){
      // test for user input InvalidArgumentException
      require "server.php";
      $_SESSION['in_kilo'] = $mysqli->escape_string($_POST['in_kilo']);
      $mysqli->close();


      if(!empty($_POST['in_kilo']))
      {
          $allGood = true;
          // echo 'allgood';
      }else{
          $allGood = false;
          // echo 'all NO good';
      }

      if($allGood === true){
        require "server.php";

        $in_fechaPago = $mysqli->escape_string(new DateTime("now", new DateTimeZone('America/Caracas')));
     //   $in_fechaPago = $in_fechaPago->format('Y-m-d H:i:s');
        $in_banco = "N/A";
        $in_cedula = "N/A";
        $in_referencia = "N/A";
        $in_moneda = "N/A";
        $in_monto = doubleval(htmlentities(0.0));
        $in_kilo = $_SESSION['in_kilo'];
        $evento_id = $eventosPDA[$keyEvento]['id'];
        // echo '<pre>';
        // var_dump($_SESSION);
        // echo '</pre>';
        // echo "go on and add the row in eventoPago ( update the 'asistirMision' to 1");
        $sql = "INSERT IGNORE INTO eventoPago (misioneroID, eventoID, fechaPago, nombreBanco, cedulaTitular, nroReferencia, tipoMoneda, monto, kilo)
         VALUES ('$id','$evento_id','$in_fechaPago', '$in_banco', '$in_cedula', '$in_referencia', '$in_moneda', '$in_monto', '$in_kilo'); ";
        $result = $mysqli->query($sql) or die($mysqli->error);
        // var_dump($result);
        // die;

        // este es para que a luisRafael le salga en una columna el listado de misioneros que se han inscrito
        $sql = "UPDATE usuario SET asistir_mision=1 WHERE usr_id='$id';";
        $result = $mysqli->query($sql) or die($mysqli->error);

        // avisarle a descarga para que diga que este usuario ha sido modificado
        $sql1 = "UPDATE descarga SET actualizacion=1 WHERE usr_id='$usr_id'; ";
        $mysqli->query($sql1) or die($mysqli->error);

        //TODO: enviar correo reportandole a la gente lo que coloco.





        $_SESSION['msg'] = '<div class="popup-message">'.$_SESSION['inscripcionAbierta']['msg'].'</div>';
        $mysqli->close();
        redirect(0,"session.php");
        exit;
      }else{
        $msg = '<p style="color:red;">Llene todo el formulario</p>';
      }
    }


    //if a valid user then I check for inactivity?
    if(isset($_SESSION['usr_id']))
    {
      if(time() - $loggedInTime > 1800) // time in seconds 1800 for 30min
      {
        header("Location: logout.php");
      }else{
        $_SESSION["loggedIn"] = time();
      }
    }


?>

<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <?php include("../css/style_config.php") ?>
    <title><?php echo $eventosPDA[$keyEvento]['name'] ?></title>
</head>
<body>
<nav class="nav-text naveg-bar custom-nav">
		<ul class="nav justify-content-between">
			<li>Hola <em><?= $email ?></em>,</li>
            <li><a type="button" class="btn btn-danger btn-sm" href="logout.php">Cerrar Sesión <i class="fas fa-sign-out-alt"></i></a></li>
        </ul>
        <div class="small text-right">Su sesión expirará luego de <span id='loggedIn'>30 min</span> de inactividad.</div>
</nav>

<div align="center">
    <a type="button" class="btn btn-warning btn-sm" href=""><i class="fas fa-sync-alt"></i> Sesión</a>
</div>

<main class="content-start">

  <div class="container">
      <div class="row justify-content-center">
          <div class="col-md-6 col-md-offset-3" align="center">
                  <h1><?php echo $eventosPDA[$keyEvento]['name'] ?></h1>
                  <h4>Proyecto de Amor</h4>
                  <br/><br/>
          </div>
      </div>
  </div>

  <div class="container">
      <div class="row justify-content-center">
          <div class="" align="center">
                  <?= $msg ?>
                  <div class="col-md-10 text-centered"><?= $msg2?></div>
                  <br/>
          </div>
      </div>
  </div>

<?php
  include("p_condicionesMisiones.php");
  include("p_agregar_kilo.php");
?>

</main>
<?php include("../inc/footer.php") ?>
<script>
let minutes = 30;
let seconds = 0;
setInterval(() => {
  if(seconds == 0){
    if(minutes == 0){
      console.log('stop the counter'); //clearInterval();
    }else{
      seconds =59;
      minutes = minutes - 1;
    }
  }else{
    seconds = seconds - 1;
  }

  document.querySelector('#loggedIn').innerText = "(" + minutes + ":" + seconds + ")";
},1000);
</script>
</body>
</html>
