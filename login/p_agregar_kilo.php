<div class="container">
    <div class="row content-justified font-italic">

        <div class="col-12" align="left">

            <form method="POST" action="">
            <input type="text" value=<?=$id?> hidden>

            <div class="ss-form-question errorbox-good" role="listitem">
              <div dir="auto" class="ss-item ss-item-required ss-text">
                <div class="ss-form-entry">
                  <label class="ss-q-item-label" for="in_cedula">
                    <div class="ss-q-title">Kilo que aportarás
                    <span class="ss-required-asterisk" aria-hidden="true">*</span>
                    </div>
                    <div class="ss-q-help ss-secondary-text" dir="auto">Coloca de que será el kilo que aportarás para formalizaar tu inscripción. Ej: Harina, arroz, aceite, leche, avena, azúcar, margarina, etc... </div>
                  </label>
                  <input type="text" name="in_kilo" oninput="isvalid(this.id,'any')" value="" class="ss-q-short" id="in_kilo" dir="auto" aria-label="Kilo de aporte" aria-required="true"  title="Kilo de aporte">
                  <span class="required-message" ></span>
                  <br/>
                  <div class="error-message" id="in_kilo_errorMessage"></div>
                  <div class="required-message" hidden>Campo requerido</div>
                </div>
              </div>
            </div>


            <input type="submit" name="kilo-submit" value="Acepto" id="ss-submit" class="btn btn-primary"/>
            <input type="submit" name="cancel" value="No Acepto" id="ss-cancel" class="btn btn-secondary"/>
            </form>
        </div>
    </div>
</div>
