<?php
require_once "repeat.php";
require_once "../messages/messages.php";
if(!isset($_SESSION))
    session_start();

if(!isset($_SESSION['urlToResetPass']))
    redirect(0,"login.php");

$email_error = $pass_error = $passC_error = "";
$pass = $passC = '';
$email = $_SESSION["email"];
$ci  = $_SESSION['cedula'];
$id = $_SESSION["usr_id"];
$msg = $_SESSION["msg"];

$failure = 0;
// print_r($_SESSION);

if(isset($_POST['submit']))
{
    if ( !empty($_POST['entry_password']) && !empty($_POST['entry_passwordConfirmed']) ) 
    {
        require_once "server.php";
        $pass = $mysqli->real_escape_string($_POST['entry_password']);
        $passC = $mysqli->real_escape_string($_POST['entry_passwordConfirmed']);
        
        if (!preg_match(regexfor('pass-w'),$pass)) {
            $pass_error = "Debe contener contener mínimo 8 caractéres";
            $failure = $failure +1;
        }
        else{
            if(strcmp($pass,$passC) != 0){
                $passC_error = "Las contraseñas deben ser idénticas";
                $failure = $failure +1;
            }else{
                $hashed_pass = password_hash(test_input($_POST["entry_password"]),PASSWORD_BCRYPT);
            }
        }
        
        if ($failure == 0)
        {
            $sql = " SELECT usr_id FROM login WHERE email='$email'; ";
            $result = $mysqli->query($sql);
            if($result->num_rows > 0){
                //conseguimos el usuario con ese correo
                $mysqli->query("UPDATE login SET is_reset=1, token='', hash = '$hashed_pass' WHERE email='$email';");
                                
                $title='Cambio de contaseña exitoso';
                $msg = '<p style="color:green;">Tu contraseña ha sido cambiada exitosamente.  Recueda colocarla en un lugar seguro para que no la pierdas.</p>';
                include "registroMsg.php";
                // cierra la session y la conexion con mysql
                session_unset();
                session_destroy();
                unset($_POST);
                $mysqli->close();
                die;
            }
            else{
                // no hay nongun usuario con ese correo
                $mysqli->close();
                redirect(0,"index.php");
            }
        }else{
            //hay errores de validación
            $msg = '<p style="color:#f44336;"></p>';
        }
        $mysqli->close();
    }else{
        //uno de los campos o los dos no estan llenos
        $msg = '<p style="color:#f44336;">Por favor llene los campos.</p>';
    }
        
}
 
?>

<!DOCTYPE html>
<html>
<head>
    <?php include("../css/style_config.php") ?>
    <title>Restauración de contraseña</title>
</head>
<body>
<?php include("../inc/navigation.php") ?>
<main class="content-start">
<div class="container">
<div class="row justify-content-center">
<div class="col-md-6 col-md-offset-3" align="center">
<h1>Proyecto de Amor</h1>
<h4>Restauración de Contraseña</h4>

<img class="img-fluid" width="20%" src="../images/logo.png" alt="PDA logo"><br><br>

<?= $msg ?>
<p>Esribe tu nueva contraseña.  Recuerda que ésta debe tener al menos 8 caractéres.</p>

<form method="POST" action=<?= $_SESSION['urlToResetPass']?>>
    <input type="text" name="user_id" value="<?=$id?>" hidden/>
<br/>
<div>
    <!-- <label for="pass">Contraseña: </label> -->
    <input class="form-control" oninput="isvalid(this.id,'pass')" type="password" name="entry_password" id="pass" placeholder="Contraseña..." value="" ><span style="color:#f44336;"><?=$pass_error?></span><br/>
    <input class="form-control" oninput="doesmatch(this.id,'pass')" type="password" name="entry_passwordConfirmed" id="passConf" placeholder="Confirma contraseña..." value="" ><span style="color:#f44336;"><?=$passC_error?></span>
    <div class="required-message" hidden>Campo requerido</div>
</div>
<br/>
    <input type="submit" name="submit" value="Guardar">
</form>
</div></div></div>

</main>
<?php include("../inc/footer.php") ?>
</body>
</html>