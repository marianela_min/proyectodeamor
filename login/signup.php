<?php
require_once "repeat.php";


// stop allowing registration
// redirect(0,urlencode("index.php"));
// die;


require_once "../messages/messages.php";
if ( isset($_POST['cancel'] ) ) {
    // Redirect the browser to login.php
    redirect(0,urlencode("../index.php"));
    return;
}

use PHPMailer\PHPMailer\PHPMailer;
//variables that fall through into the html

$failure = 0;
$msg = "";
$name_error = $email_error = $pass_error = $passC_error = $ci_error = "";
$name = $email = $pass = $passC = $ci= '';


if(isset($_POST['submit']))
{
    // echo '<pre>';
    // echo var_dump($_POST);
    // echo '</pre>';
    // Check to see if we have some POST data, if we do process it
    if ( !empty($_POST['entry_email']) || !empty($_POST['entry_password']) || !empty($_POST['entry_passwordConfirmed']) || !empty($_POST['entry_name']) )
    {
        require_once "server.php";

    $name = ucwords(strtolower($mysqli->real_escape_string($_POST['entry_name'])));  // Escape, set to lowecase then set first letter to uppercase
    $ci = intval($_POST['entry_cedula']);
    $email = strtolower($mysqli->real_escape_string($_POST['entry_email'])); //Escape and set everything to lowercase
    $pass = $mysqli->real_escape_string($_POST['entry_password']);
    $passC = $mysqli->real_escape_string($_POST['entry_passwordConfirmed']);

    //matches that my name contains only A-Z a-z and up to 4 words with spaces in between
    if (!preg_match(regexfor('ful-name'),$name)) {
        $name_error = "Sólo letras y espacios están permitidos";
        $failure = $failure +1;
    }

    if (!valid_ci($ci)) {
     $ci_error = "Sólo dígitos, no coloque ningún signo de puntuación (.,-)";
     $failure = $failure +1;
 }

      //matches a valid email: starting with a letter [a-z] followed by any word character followed by an @
      //followed by another letter [a-z] or dot or hyphen one or more times until it finishes with
      // a dot (.) followed by 2, 3 or 4 letters [a-z] AND that is the end of the line
 if (!preg_match(regexfor('email'),$email)) {
    $email_error = "Ingresa un correo válido";
    $failure = $failure +1;
}

       //matches any character string having a length of at least 7.  They must have at least
       // 1 number [0-9], 1 uppercase, 1 lowercase and no spaces
if (!preg_match(regexfor('pass-w'),$pass)) {
    $pass_error = "Debe contener contener mínimo 8 caracteres";
    $failure = $failure +1;
}
else{
    if(strcmp($pass,$passC) != 0){
        $passC_error = "Las contraseñas deben ser idénticas";
        $failure = $failure +1;
    }else{
        $hashed_pass = password_hash(test_input($_POST["entry_password"]),PASSWORD_BCRYPT);
    }
}


if ($failure == 0)
{


    $sql = " SELECT * FROM ci WHERE ci='$ci'; ";
    $result = $mysqli->query($sql);
    if($result->num_rows==0){
        $misionero=0;
        $next=true;
    }else{
        $row=$result->fetch_assoc();
        if($row['deuda']==1){
            $next=false;
        }else{
            $misionero=$row['misionero_viejo'];
            $next = true;
        }
    }



    if($next===true)
    {
            // echo "procesa lo demas";
        $sql = " SELECT usr_id FROM login WHERE email='$email' OR cedula='$ci'; ";
        $result = $mysqli->query($sql);
        if($result->num_rows > 0){
                // var_dump($result);
            $msg = '<p style="color:#f44336;">El correo electrónico o número de cédula proporcionado ya está registrado</p>';
        }else{
                // echo "crear el tocken y enviar correo";
            $token = 'qwertzuiopasdfghjklyxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM0123456789!$/()*';
            $token = str_shuffle($token);
            $token = substr($token, 0, 10);
            $sql = "INSERT INTO login (email,misionero,cedula,hash,is_confirmed,token,is_reset) VALUES ('$email','$misionero','$ci', '$hashed_pass', '0', '$token','1');";
            $result = $mysqli->query($sql) or ($mysqli->error);
                // echo '<p>this is insert_id: '.$mysqli->insert_id;
                // echo 'mysqli object: '.$result;
                // echo'</p>';
            include_once "../PHPMailer/PHPMailer.php";

                // $msg = 'sending email... <br>'."<a href='https://pda.org.ve/miembro/login/confirm.php?email=$email&action=0&token=$token'>Activa tu cuenta</a>";
            $mail = new PHPMailer();
            $mail->CharSet = 'UTF-8';
            $mail->setFrom('info@pda.org.ve','Proyecto de Amor');
            $mail->addAddress($email, $name);
            $mail->Subject = "Confirma tu correo electrónico!";
            $mail->isHTML(true);
            $mail->Body =  registerMailMsg($name,$email,$token);

            if ($mail->send())
                $msg = '<p style="color: green; min-height: 30vh;">¡Que bueno!<br/>Tu cuenta ha sido creada.<br/> Ahora busca en tu bandeja de entrada el correo de activación de tu cuenta, recuerda revisar <strong>la carpeta de spam o el correo no deseado</strong>. Recomendamos revisar el correo no deseado en una computadora, en dispositivos mobiles esto puede tardar más de lo esperado.<br/><br/>Si en diez (10) minutos no has recibido el correo por favor contáctanos.</p>';
            else
                $msg = '<p style="color: #f44336; min-height: 30vh;">¡Oops! Algo extraño ocurrió.  No pudimos crear tu cuenta.  Reporta este incidente al administrador del sitio web.  Por favor, intenta de nuevo.  Gracias por tu paciencia.</p>';

                // echo $msg;
            unset($_POST);
            $title='Confirmación de correo';
            include "registroMsg.php";
            die;
        }

    }else{
        $msg = '<p style="color:#f44336;">El usuario presenta deuda con el grupo, por favor contacta a proyectodeamor@gmail.com para gestionar el pago</p>';
        $name = $email = $pass = $passC = $ci= '';
    }
    $mysqli->close();
}
}
else
{
    $msg = '<p style="color:#f44336;">Por favor ingrese todos sus datos</p>';
}

}else{
    $msg = '<p style="color:green;"></p>';
}

// Fall through into the View
?>

<!DOCTYPE html>
<html>
<head>
    <?php include("../css/style_config.php") ?>
    <link rel="stylesheet" href="../css/generalStyle.css">
    <title>Crea tu cuenta - Proyecto de amor</title>
    <script>
        function copyOver(id,id2Copy){
            document.getElementById(id2Copy).innerText = document.getElementById(id).value;
        }
    </script>
</head>
<body>
    <?php include("../inc/navigation.php") ?>
    <main class="content-start">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 col-md-offset-3" align="center">
                    <h1>Proyecto de Amor</h1>
                    <h4>Por favor ingresa tus datos</h4>
                    <img class="img-fluid" width="20%" src="../images/logo.png" alt="PDA logo"><br><br>
                    <p>Pon especial atención en la cédula y el correo electrónico, pues de colocarlos errados no tendrás oportunidad de modificarlo.</p>
                    <?php if ( $failure == ""){
                        echo($msg);} ?>

                        <form method="POST" action="signup.php">
                            <div>
                                <input class="form-control" oninput="isvalid(this.id,'ful-name')" type="text" name="entry_name" id="name" placeholder="Nombre..." value="<?=$name?>" notrequired><span style="color:#f44336;"><?=$name_error?></span>
                                <div class="required-message" hidden>Campo requerido</div>
                            </div>
                            <br/>
                            <div>
                                <input class="form-control" oninput="isvalid(this.id,'cedula');copyOver(this.id,'copy_entry_cedula')" type="number" name="entry_cedula" id="cedula" placeholder="Cedula..." value="<?=$ci?>" min=100000 max=50000000 notrequired><span style="color:#f44336;"><?=$ci_error?></span>
                                <div class="required-message" hidden>Campo requerido</div>
                            </div>
                            <br/>
                            <div>
                                <!-- <label for="email">Email: </label> -->
                                <input class="form-control" type="text" oninput="isvalid(this.id,'email');copyOver(this.id,'copy_entry_email')" name="entry_email" id="email" placeholder="Email..." value="<?=$email?>" notrequired><span style="color:#f44336;"><?=$email_error?></span>
                                <div class="required-message" hidden>Campo requerido</div>
                            </div>
                            <br/>
                            <div>
                                <!-- <label for="pass">Contraseña: </label> -->
                                <input class="form-control" oninput="isvalid(this.id,'pass-w')" type="password" name="entry_password" id="pass" placeholder="Contraseña..." value="" notrequired><span style="color:#f44336;"><?=$pass_error?></span><br/>
                                <input class="form-control" oninput="doesmatch(this.id,'pass')" type="password" name="entry_passwordConfirmed" id="passConf" placeholder="Confirma contraseña..." value="" notrequired><span style="color:#f44336;"><?=$passC_error?></span>
                                <div class="required-message" hidden>Campo requerido</div>
                            </div>
                            <br/>
                            <button type="button" class="btn btn-primary btn-lg " data-toggle="modal" data-target="#verifRegistro">Registrar tu cuenta</button>

                            <div class="modal fade" id="verifRegistro" role="dialog">
                                <div class="modal-dialog" align="left">

                                  <!-- Modal content-->
                                  <div class="modal-content">
                                    <div class="modal-header">

                                      <h4 class="modal-title">¿Seguro?</h4>
                                  </div>
                                  <div class="modal-body">
                                    <p>Al registrar tu cuenta la cédula <strong style="color:red;">V-<span id="copy_entry_cedula">XXXXXXXX</span></strong> será asociada con el correo electrónico <strong style="color:red;"><span id="copy_entry_email">coloca@tucorreo.com</span></strong>.</p>
                                    <p>Para activar tu cuenta se te enviará un correo de confirmación al correo electrónico que nos indicaste.  En el caso de que el correo o la cédula no sean correctos, se te bloqueará tu cuenta y no podrás activarla ni acceder a ella.</p>
                                </div>
                                <div class="modal-footer">
                                <p class="small">¿Tus datos fueron colocados correctamente?</p>
                                  <input type="submit" name="submit" class="btn btn-primary" value="Si, correcto">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Regresar</button>
                              </div>
                          </div>

                      </div>
                  </div>






              </form>
          </div></div></div>
      </main>
      <?php include("../inc/footer.php") ?>
  </body>
  </html>
