function add(text_id,input_id){
    var value_c = document.getElementById(text_id).value;
    var regex_c = new RegExp("\,?"+value_c);
    var value_i = document.getElementById(input_id).value;
    if(regex_c.test(value_i)){
        // console.log('it matches so erase it, replace with \'\'');
        document.getElementById(input_id).value = value_i.replace(regex_c,'').replace(/^\,/,'');
    }else{
        // console.log('it does not match so add it with append');
        if(value_i == '')
            document.getElementById(input_id).value += value_c;
        else
            document.getElementById(input_id).value += "," +value_c;
    }
}


function setval(text_id, input_id) {
    var value_c = document.getElementById(text_id).value;
    var regex_c = new RegExp("\,?" + value_c);
    var value_i = document.getElementById(input_id).value;
    if (regex_c.test(value_i)) {
        document.getElementById(text_id).checked = true;
    }
    else{
        document.getElementById(text_id).checked = false;
    }
}


function setrad(text_id, seleccion) {
    var rad_obj = document.getElementById(text_id);
    // console.log("set rad executing");
    // console.log("Este es value:"+rad_obj.value+"  este es el sexo:"+seleccion);
    if(rad_obj.value == seleccion){
        //  console.log("equal");
         rad_obj.checked = true;
    }else{
        //  console.log("not equal");
         rad_obj.checked = false;
    }
}





function regexfor($s){
    switch ($s) {
        case 'ful-name':
        //matches that my name contains only A-Z a-z and up to 4 words with spaces in between
            $regEx = "^[A-Za-zñáéíóúÑÁÉÍÓÚ\\/]+\\s?[A-Za-zñáéíóúÑÁÉÍÓÚ\\/]*\\s?[A-Za-zñáéíóúÑÁÉÍÓÚ\\/]*\\s?[A-Za-zñáéíóúÑÁÉÍÓÚ\\/]*$";
            break;
        case 'name':
            $regEx = "^[A-Za-zñáéíóúÑÁÉÍÓÚ\\/]+$";
            break;
        case 'email':
            $regEx = "^(:?[a-z][\\w\\.\\-]*)@[a-z][\\w\\.\\-]*?(=?\\.[a-z]{2,4})$";
            break;
        case 'pass-w':
            $regEx = "^.{8,}$";
            break;
        case 'pass':
            $regEx = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?!.*[\\s]).{7,}$";
            break;
        case 'phone':
            $regEx = "^\\d{4}\\-\\d{7}$";
            break;
        case 'dob':
            $regEx = "^((:?19|20)\\d{2})\\-?([01]\\d)\\-?([0123]\\d)$"; // YYYY-MM-DD
            break;
        case 'cedula':
            $regEx = "^\\d{6,11}$";
        case 'numero':
            $regEx = "^\\d{10,11}$";
            break;
        case 'any':
            $regEx = "^[ñáéíóúÑÁÉÍÓÚ\\w\\s\\(\\)\\,\\#\\.\\-\\/\\+]{1,200}$"; //this is anything
            // echo "regex any en uso:  ".$regEx."\n";
            break;

        default:
            $regEx = "^[ñáéíóúÑÁÉÍÓÚ\\w\\s\\(\\)\\,\\#\\.\\-\\/]{0,90}$";
            break;
    }

return new RegExp($regEx);
}
function doesmatch(id,id_m){
    var obj = document.getElementById(id);
    var mat = document.getElementById(id_m);
    // console.log(id.value == mat.value+" "+mat.value);
        showerror(obj,obj.value == document.getElementById(id_m).value);
}

function isvalid(id,type){
    var obj = document.getElementById(id);
   var expression = regexfor(type);
    var valid;
if(type == 'cedula'){
    valid = isvalid_ci(obj.value);
    }
    else if(type == 'select'){
        valid = (obj.selectedIndex==0)?false:true;
    }else{
        valid = expression.test(obj.value.trim());
// console.log(expression);
// console.log(obj.value);
    }
    showerror(obj,valid);
}

function showerror(obj,valid)
{
    if(valid){
        obj.removeAttribute("style");
        obj.parentElement.lastElementChild.hidden = true;
        obj.nextSibling.innerText = "";
        // console.log("Pepa");
    }else{
        // console.log("corrija este campo");
        obj.setAttribute("style","background-color: #ffefee; color: red;");
        obj.parentElement.lastElementChild.hidden = false;
        error = '<i class="fas fa-exclamation-circle"></i>';
    }
}



function isvalid_ci($numero){
    $min = 1000000; // un millon
    $max = 50000000; // 50 millones
    if( $numero > $min && $numero < $max )
        return true;
    else
        return false;
}


function menor(date_id,block_id)
    	{
    var obj = document.getElementById(date_id);
    var block = document.getElementById(block_id);
    var inp = block.getElementsByTagName('input');
    var expression = new RegExp("^((:?19|20)\\d{2})\\-?([01]\\d)\\-?([0123]\\d)$"); // YYYY-MM-DD
    var val = obj.value.match(expression);
    if (val !== null)
    {
        var yy = parseInt(val[1]);
        var mm = parseInt(val[3]);
        var dd = parseInt(val[4]);
    	// console.log("year: "+yy+" month: "+mm+" day: "+dd);

      // year from which kids are mayor de edad, adults.
        let d = new Date();
        let mayor_de_edad = d.getFullYear()-18;
        if(yy<mayor_de_edad || (yy<=mayor_de_edad && mm<3)){
        	// console.log("Mayor de edad");
        	inp.RepTel.value = "n/a";
        	inp.RepNombre.value = "n/a";
            inp.RepParentesco.value = "n/a";
            block.hidden = true;
        }else{
        	inp.RepTel.value = "";
        	inp.RepNombre.value = "";
            inp.RepParentesco.value = "";
            block.hidden = false;
        }
	}
}

function setsel(obj_id,valor){
    var hijos = document.getElementById(obj_id).children;
    for (var i = 1; i < hijos.length; i++) {
            if(hijos[i].value == valor){
            // console.log("i: "+i+".   Este es el hijo: "+hijos[i]);
                hijos[i].selected = true;
            }else{
                hijos[i].selected = false;
            }
        }
}
