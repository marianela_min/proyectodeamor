<?php

require_once "repeat.php";
if ( isset($_POST['cancel'] ) ) {
    // echo("Aca entro y quiere redireccionar a signup;");
    redirect(-1,"index.php");
    exit;
}
use PHPMailer\PHPMailer\PHPMailer;
//variables that fall through into the html

$failure = 0;
$msg = "";
$name_error = $email_error = $pass_error = $passC_error ="";
$name = $email = $pass = $passC = '';

if(isset($_POST['submit']) )
{
    // echo '<pre>';
    // echo var_dump($_POST);
    // echo '</pre>';
    // Check to see if we have some POST data, if we do process it
    if ( !empty($_POST['entry_email']) || !empty($_POST['entry_password']))
    {
        require_once "server.php";
        $email = trim(strtolower($mysqli->escape_string($_POST['entry_email']))); //Escape and set everything to lowercase and trims
        $pass =  $mysqli->escape_string($_POST['entry_password']);
        
        //matches a valid email: starting with a letter [a-z] followed by any word character followed by an @
        //followed by another letter [a-z] or dot or hyphen one or more times until it finishes with
        // a dot (.) followed by 2, 3 or 4 letters [a-z] AND that is the end of the line
        if (!preg_match(regexfor('email'),$email)) {
        $email_error = "Ingresa un email válido";
        $failure = $failure +1;
    }
    
    //matches any character string having a length of at least 7.  They must have at least
    // 1 number [0-9], 1 uppercase, 1 lowercase and no spaces  
    // if (!preg_match(regexfor('pass'),$pass)) {
    //     $pass_error = "Debe contener contener minimo 7 caracteres,<br> entre los cuales debe haber 1 letra minuscula[a-z], 1 letra mayuscula [A-Z] y 1 numero[0-9]";
    //     $failure = $failure +1;
    // } 
    // if (!preg_match(regexfor('pass-w'),$pass)) {
    //     $pass_error = "Debe contener contener mínimo 8 caracteres";
    //     $failure = $failure +1;
    //   }

    
    if ( $failure == 0 )
    {
        
        
        $sql = " SELECT * FROM login WHERE email='$email';";
        $result = $mysqli->query($sql);
        if($result->num_rows > 0)
        {    
            $row =  $result->fetch_assoc();
            // echo "we have that email";
            // echo '<pre> DB: ';
            // echo var_dump($row);
            // echo '</pre>';
            
            if($row['is_confirmed']==0){
                // echo '<p style="color:green;">Porfavor confirme su cuenta primero. Verifique su correo!</p>'; 
                $msg = '<p style="color:#ffc107;"><i class="fas fa-exclamation-triangle"></i> Para ingresar al sistema usted debe confirmar su cuenta primero. Por favor revise la bandeja de entrada de su correo.  No olvide chequear la bandeja de correo no deseado!</p>';
                
            }else{
                

                if(password_verify(test_input($_POST["entry_password"]), $row['hash']) === false)
                {
                    $msg = '<p style="color:#f44336;">contraseña o correo invalidos</p>';   
                }
                else{        
                    if(!isset($_SESSION))
                    session_start();
                    // echo '<pre> SESSION: ';
                    // echo var_dump($_SESSION);
                    // echo '</pre>';               
                    
                    
                    $_SESSION["email"]=$row['email'];
                    $_SESSION['cedula']=$row['cedula'];
                    $_SESSION["usr_id"]=$row['usr_id'];
                    $_SESSION["pass"]=$row['hash'];
                    $_SESSION['msg']='';//<p style="color:green;">Has iniciado sesión exitosamente.</p>';
                    $_SESSION['loggedIn'] = time();
                    $sql = "UPDATE login SET token='', is_reset=1 WHERE email='$email';";
                    $result = $mysqli->query($sql) or ($mysqli->error);
                    
                    redirect(0,"session.php");
                    exit;
                    // die;
                }
            }
        }else{
            $msg = '<p style="color:#f44336;">El correo no ha sido registrado</p>';
         
        }
        $mysqli->close();
    }

}else
{
    $msg = '<p style="color:#f44336;">Porfavor ingrese todos sus datos</p>';
    
}

}else{
      
        $msg = '<p style="color:gray;">Ingresa tus credenciales para iniciar sesión</p>';
   
}




// Fall through into the View
?>

<!DOCTYPE html>
<html>
<head>
    <?php include("../css/style_config.php") ?>
    <title>Iniciar Sesion - Proyecto de amor</title>
</head>
<body>
<?php include("../inc/navigation.php") ?>
<main class="content-start">
<div class="container">
<div class="row justify-content-center">
<div class="col-md-6 col-md-offset-3" align="center">
<h1>Proyecto de Amor</h1>
<h4>Por favor inicia sesion</h4>


<img class="img-fluid" width="20%" src="../images/logo.png" alt="PDA logo"><br><br>

<?= $msg ?>
        
<form method="POST" action="login.php">
    <div>
        <!-- <label for="email">Email: </label> -->
        <input class="form-control" type="text" oninput="isvalid(this.id,'email')" name="entry_email" id="email" placeholder="Email..." value="<?=$email?>" required><span style="color:#f44336;"><?=$email_error?></span>
        <div class="required-message" hidden>Campo requerido</div>
    </div>
    <br/>
    <div>
        <!-- <label for="pass">Contraseña: </label> -->
        <input class="form-control" type="password" name="entry_password" id="pass" placeholder="Contraseña..." value="" required><span style="color:#f44336;"><?=$pass_error?></span>
        <div class="required-message" hidden>Campo requerido</div>
    </div>
    <br/>
    <input type="submit" class="btn btn-success btn-lg" name="submit" value="Inicia Sesión">
    <!-- <input type="submit" name="cancel" value="Cancel"> -->
</form>
<p>
<a href="resetpass.php?rest=1">Olvidó la contraseña</a>
</p>
</div>
</main>
<?php include("../inc/footer.php") ?>
</body>
</html>