<?php

/*This guy will handle links coming from an email sent to the user.*/
require_once "repeat.php";
$title='redirect';
	if (!isset($_GET['email']) || !isset($_GET['token']) || !isset($_GET['action'])) {
		// echo '<h1 style="color:purple;">TO-DO</h1>';
		// echo "Nada que hacer... redireccionando a la pagina principal..";
		redirect(-1,"index.php");
	} else {
		require "server.php";

		$email = $mysqli->real_escape_string($_GET['email']);
		$action = intval($_GET['action']);
		$token = $mysqli->real_escape_string($_GET['token']);

		//confirmar la cuenta para que pueda iniciar sesion.
		$result = $mysqli->query("SELECT * FROM login WHERE email='$email'");
		if ($result->num_rows > 0) {
			// echo "There is an email that matches";
			$row = $result->fetch_assoc();
			if($action==0 && $row['is_confirmed']==0 && $row['token']==$token){
				$title = 'Confirmación';
				$mysqli->query("UPDATE login SET is_confirmed=1, token='' WHERE email='$email'");
				$msg = '<p style="color: green; min-height: 30vh;">¡Su cuenta con '.$email.' ha sido activada exitosamente!</p>';
				include "registroMsg.php";
				$mysqli->close();
				die;
				
			}else if($action==0 && $row['is_confirmed']==1){
				$title='Confirmación';
				$msg = '<p style="color: green; min-height: 30vh;">¡Su cuenta con '.$email.' ya ha sido activada! Ahora puede iniciar sesión.</p>';
				include "registroMsg.php";
				$mysqli->close();
				die;

			}else if($action==1 && $row['is_reset']==0 && $row['token']==$token){
				$msg = '<p style="color: green;">Escribe tu nueva contraseña</p>';
				if(!isset($_SESSION))
                    session_start();
                // echo '<pre> SESSION: ';
                // echo var_dump($_SESSION);
				// echo '</pre>';         
				      
				$_SESSION["email"]=$row['email'];
				$_SESSION['cedula']=$row['cedula'];
                $_SESSION["usr_id"]=$row['usr_id'];
                $_SESSION['msg']=$msg;
				$_SESSION['urlToResetPass'] = "confirm.php?email=$email&action=1&token=$token";
			    $_SESSION['loggedIn'] = time();
                // redirect(0,"typenewpass.php");
				include "typenewpass.php";
				$mysqli->close();
				die;
			}else{
				// echo '<h1 style="color:purple;">TO-DO</h1>';
				// echo "Email and token match but no need for action. or broken url\n";
				// echo "redirect to homepage\n";
				// echo '<a href="../index.php">Volver a la pagina principal</a>';
				$mysqli->close();
				redirect(-1,"index.php");
			}
		}else{
				// echo '<h1 style="color:purple;">TO-DO</h1>';
				// echo "Broken url. or nothing on DB\n";
				// echo "redirect to homepage\n";
				// echo '<a href="../index.php">Volver a la pagina principal</a>';
				$mysqli->close();
				redirect(-1,"index.php");
				
		}
	$mysqli->close();
	}