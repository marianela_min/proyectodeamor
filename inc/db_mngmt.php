<?php

function estaSolvente($userCi, $db_handler){
    //returns a boolean
    $estaSolvente = true;

    $sql = " SELECT * FROM ci WHERE ci='$userCi'; ";
    $result = $db_handler->query($sql);
    if($result->num_rows!=0){
        $row=$result->fetch_assoc();
        if($row['deuda']==1){
            $estaSolvente = false;
        }
    }
    return boolval($estaSolvente);
}

function descargaDatos($db_handle,$descarga_all){
    //filename is the entire path of where I want the file to be saved!
    // $filename = 'misioneros_'.date("YMdhis").'.csv';
    $filename = '../descargas/'.'misioneros_.csv';

    if($descarga_all){
        $sql = "SELECT usr_id FROM descarga; ";
    }
    else{
        $sql = "SELECT usr_id FROM descarga WHERE actualizacion='1'; ";
    }
    $result = $db_handle->query($sql) or ($db_handle->error);
    // print_r($result);
    if($result->num_rows==0){
        $needTodownload = false;
        $button = '<a role="button" class="btn btn-secondary disabled">Nada para Descargar</a>';
    }else{

        $needTodownload = true;
        $list = getUsrDataFromDb($db_handle,$result);
        // echo "<pre>";
        // var_dump($list);
        // echo "</pre>";
        $fp = fopen($filename, 'wb');
        fprintf($fp, chr(0xEF).chr(0xBB).chr(0xBF));
        foreach($list as $fields)
             fputcsv($fp, $fields);

        fclose($fp);

        // $button = '<a role="button" class="btn btn-primary" href="'.$filename.'">Descarga</a>';

        //set the fields as read
        $sql = "UPDATE descarga SET actualizacion=0 WHERE actualizacion='1'; ";
        $result = $db_handle->query($sql) or ($db_handle->error);

        header("Location: ".$filename);
    }
        return $button;
}

function getUsrDataFromDb($db_handle, $users2download){
    $usersData = array(array('asistir_mision','primer_name', 'segun_name', 'primer_last', 'segun_last', 'cedula', 'talla', 'sexo', 'dob', 'rep_name', 'rep_tel', 'rep_parentesco', 'tel_fijo', 'tel_mobil', 'email', 'calle_dir', 'estudia_donde', 'estudia_que', 'trabaja_donde', 'profesion', 'sacramentos', 'grupos_catolic', 'grupos_nocatolic', 'referidode', 'misiones', 'novio_a', 'talentos', 'otro_tal', 'sangre', 'enfermedad', 'medicina', 'alergia', 'full_nombre_p', 'telefono_p', 'full_nombre_m', 'telefono_m', 'vehiculo', 'emigrar'));

    while($row = $users2download->fetch_assoc())
    {
      $id = $row['usr_id'];
        // echo 'user_id: '.$id.'--';
      //the following lines bring all the user's info from the DB in one array.

      // una forma de colocarle a luisra el kilo incluido en la hoja de excel general.

      // select eventoPago.kilo, matrix.* From (SELECT login.email,login.cedula, u.*, p.*, ed.*, em.*, s.*
      //     FROM usuario as u, salud as s, personal as p, emergencia as em, educacion as ed, login
      //     where u.usr_id=1 and p.usr_id=1 and ed.usr_id=1 and em.usr_id=1 and s.usr_id=1 and login.usr_id=1) as matrix
      //     left join eventoPago on eventoPago.misioneroID = matrix.u.usr_id;

      $sql = "SELECT login.email,login.cedula, u.*, p.*, ed.*, em.*, s.*
      FROM usuario as u, salud as s, personal as p, emergencia as em, educacion as ed, login
      WHERE u.usr_id='$id' and p.usr_id='$id' and ed.usr_id='$id' and em.usr_id='$id' and s.usr_id='$id' and login.usr_id='$id';";
      $result = $db_handle->query($sql);
      if($result->num_rows>0){
        // echo 'existe la busqueda<br/>';
        $user = $result->fetch_assoc();
        $user_cust = array($user['asistir_mision'],$user['primer_name'], $user['segun_name'], $user['primer_last'], $user['segun_last'], $user['cedula'], $user['talla'], $user['sexo'], $user['dob'], $user['rep_name'], $user['rep_tel'],
           $user['rep_parentesco'], $user['tel_fijo'], $user['tel_mobil'], $user['email'], $user['calle_dir'], $user['estudia_donde'], $user['estudia_que'], $user['trabaja_donde'], $user['profesion'], $user['sacramentos'], $user['grupos_catolic'],
            $user['grupos_nocatolic'], $user['referidode'], $user['misiones'], $user['novio_a'], $user['talentos'], $user['otro_tal'], $user['sangre'], $user['enfermedad'], $user['medicina'], $user['alergia'], $user['full_nombre_p'],
             $user['telefono_p'], $user['full_nombre_m'], $user['telefono_m'], $user['vehiculo'], $user['emigrar']);
        array_push($usersData,$user_cust);
      }


    }
    // echo "<pre>";
    // var_dump($usersData);
    // echo "</pre>";
    return $usersData;
}


function get_ci($db_handle){
    $usersData = array();
    $sql = "SELECT ci_id, ci FROM ci WHERE deuda=1 ORDER BY ci ASC; ";
    $result = $db_handle->query($sql) or ($db_handle->error);

    while($row = $result->fetch_assoc())
    {
      $user_cust['ci_id'] = $row['ci_id'];
      $user_cust['ci'] = $row['ci'];
      array_push($usersData,$user_cust);
    }
    return $usersData;
}


function eraseCiFrom($db_handle,$id){
    // echo $id;
    $sql = "SELECT ci FROM ci WHERE ci_id = $id; ";
    $result = $db_handle->query($sql) or ($db_handle->error);
    if($result->num_rows==0){
        // echo "nothing to do";
        $returnMsg = '<p style="color:gray;">Nada para borrar</p>';
    }else{
        $row = $result->fetch_assoc();
        $deletedCi = $row['ci'];
        $sql = "UPDATE ci SET deuda=0 WHERE ci_id = $id; ";
        $result = $db_handle->query($sql) or ($db_handle->error);
        $returnMsg = '<p style="color:green;">Cedula: <strong> V-'.$deletedCi.'</strong> borrada exitosamente</p>';
    }
    return $returnMsg;
}


function getInscritos($db_handler, $eventoID){
    $sql = " SELECT * FROM (SELECT l.usr_id, l.cedula, u.primer_name, u.primer_last FROM login as l inner join usuario as u on l.usr_id = u.usr_id) as info inner join eventoPago as a ON info.usr_id = a.misioneroID and a.eventoID = '$eventoID' ORDER BY info.primer_last, info.primer_name;";
    // $sql = "SELECT count(*) FROM login as l INNER JOIN usuario as u ON l.usr_id=u.usr_id WHERE l.misionero=0 and u.asistir_mision=1";

    $result = $db_handler->query($sql);
    $cupos = $result->num_rows;
    // var_dump($result);
    // echo $result->field_count;
    // die;
    return $cupos;
    }
function n_getInscritos($db_handler, $eventoID){
    // $sql = "SELECT l.misionero, u.asistir_mision FROM login as l INNER JOIN usuario as u ON l.usr_id=u.usr_id WHERE l.misionero=0 and u.asistir_mision=1";
    $sql = "SELECT * FROM
    (SELECT l.usr_id, l.cedula, u.primer_name, u.primer_last FROM login as l inner join usuario as u ON l.usr_id = u.usr_id and l.misionero=0) as info
            inner join eventoPago as a ON info.usr_id = a.misioneroID and a.eventoID = '$eventoID' ORDER BY info.primer_last, info.primer_name;";

    $result = $db_handler->query($sql);
    $cupos = $result->num_rows;
    return $cupos;
}

function v_getInscritos($db_handler, $eventoID){
    // $sql = "SELECT l.misionero, u.asistir_mision FROM login as l INNER JOIN usuario as u ON l.usr_id=u.usr_id WHERE l.misionero=1 and u.asistir_mision=1";
    $sql = "SELECT * FROM
    (SELECT l.usr_id, l.cedula, u.primer_name, u.primer_last FROM login as l inner join usuario as u on l.usr_id = u.usr_id and l.misionero=1) as info
     inner join eventoPago as a ON info.usr_id = a.misioneroID and a.eventoID = '$eventoID' ORDER BY info.primer_last, info.primer_name;";
    $result = $db_handler->query($sql);
    $cupos = $result->num_rows;
    return $cupos;

}

function get_unConfirmed($db_handle){
    $usersData = array();
    $sql = "SELECT usr_id, cedula, email FROM login WHERE is_confirmed=0 AND token!= '' ORDER BY  email ASC;";
    //SELECT ci_id, ci FROM ci WHERE deuda=1 ORDER BY ci ASC; ";
    $result = $db_handle->query($sql) or ($db_handle->error);

    while($row = $result->fetch_assoc())
    {
      $user_cust['usr_id'] = $row['usr_id'];
      $user_cust['ci'] = $row['cedula'];
      $user_cust['email'] = $row['email'];
      array_push($usersData,$user_cust);
    }
    return $usersData;
}


function borrar_unConfirmed($db_handle,$usrToDel){
    // echo $id;
    $id = $usrToDel['i_id'];
    $ci = $usrToDel['i_ci'];
    $em = $usrToDel['i_em'];
    // echo "  id: ".$id."    ci: ".$ci."  email: ".$em;
    // echo '<br/>';
    $sql = "SELECT cedula, email FROM login WHERE usr_id = '$id' AND cedula='$ci' AND email='$em'; ";
    // echo $sql;
    $result = $db_handle->query($sql) or ($db_handle->error);
    // var_dump($result);
    if($result->num_rows==0){
        // echo "nothing to do";
        $returnMsg = '<p style="color:gray;">Nada para borrar</p>';
    }else{
        $row = $result->fetch_assoc();
        $ciDel = $row['cedula'];
        $emailDel = $row['email'];
        $sql = "DELETE FROM login WHERE usr_id = $id; ";
        $result = $db_handle->query($sql) or ($db_handle->error);
        $returnMsg = '<p style="color:green;">Correo electrónico: <strong>'.$emailDel.'</strong> y cédula: <strong> V-'.$ciDel.'</strong> liberados exitosamente</p>';
    }
    return $returnMsg;
}

function setMoroso($db_handle, $ci_deudores){

    if (count($ci_deudores) > 0){
      $returnMsg = '<div style="color:green;">Agregado a la lista de morosos:<br><ul>';
      foreach ($ci_deudores as $ci) {
        $sql = " INSERT IGNORE INTO ci (ci) VALUES ('$ci'); ";
        $db_handle->query($sql);
        $last_id = $db_handle->insert_id;
      }

      $i = 0;
      // echo '<ol>';
      foreach ($ci_deudores as $ci) {
          $sql = " UPDATE ci SET deuda=1 WHERE ci=$ci; ";
          $db_handle->query($sql);
          $last_id = $db_handle->insert_id;
          // if($last_id==0){
              // $db_handle->query($sql);

          // echo '<li> last ID inserted ('.$last_id.') for: '.$ci.'</li>';
          $i = $i + 1;
          // $returnMsg = $returnMsg.'<li>Cédula: <strong> V-'.$ci.'</strong> (<small></small>) </li>';
          $returnMsg = $returnMsg.'<li>Cédula: <strong> V-'.$ci.'</strong></li>';
      }
      $returnMsg = $returnMsg.'</ul></div>';

    }
    else{
      $returnMsg = '<p style="color:gray;">Nada para agregar</p>';
    }

      return $returnMsg;
}


function intent_to_update_ci_on_email($db_handle, $userToMod){
    $usersReport['exist_email'] = $curr_email = $userToMod['i_em'];
    $usersReport['exist_cedulaNew'] = $new_ci = $userToMod['i_ci'];
    $usersReport['exist_usr_id'] = '&#63;';
    $usersReport['exist_cedulaAct'] =  '&#63;';
    $usersReport['exist_deuda'] =  '&#63;';
    $usersReport['exist_misionero'] = '&#63;';
    $usersReport['exist_mensage'] = '&#63;';

    $sql = "SELECT usr_id, misionero, cedula, email FROM login where email='$curr_email' OR cedula='$new_ci';";
    // echo $sql;
    $result = $db_handle->query($sql) or ($db_handle->error);
    //checks if the email or the new cedula exist and a good case would be if only one row is returned.
    if($result->num_rows == 1)
    {
        $row = $result->fetch_assoc();
        if ($row['email'] == $curr_email)
        {
            $usersReport['exist_mensage'] = 'El correo se encuentra registrado en el sistema';
            $usersReport['exist_usr_id'] = $usr_id = $row['usr_id'];
            $usersReport['exist_cedulaAct'] = $row['cedula'];

                //searches for a whether they have a pending balance and type of misionero (old or new)
            $sql = "SELECT deuda,misionero_viejo from ci where ci='$new_ci';";
                // echo $sql;
            $result = $db_handle->query($sql) or ($db_handle->error);

            if($result->num_rows == 1)
            {
                $row = $result->fetch_assoc();
                if($row['deuda']==0){
                    $usersReport['exist_deuda'] =  0;
                    $usersReport['exist_mensage'] = 'Puedes hacer el cambio de cédula';
                    // UPDATE login SET misionero = $misionero, cedula = $new_ci where usr_id= $usr_id
                }else{
                    $usersReport['exist_deuda'] =  1;
                    $usersReport['exist_mensage'] = 'El misionero presenta deuda con el grupo';
                    // echo 'no puede realizarse nada porque debe dinero';
                }
                    $usersReport['exist_misionero'] = $misionero = $row['misionero_viejo'];

            }else if($result->num_rows == 0){
                    $usersReport['exist_deuda'] =  0;
                    $usersReport['exist_misionero'] = $misionero = 0;
                    $usersReport['exist_mensage'] = 'Puedes hacer el cambio de cédula';
                    // UPDATE login SET misionero = $misionero, cedula = $new_ci where usr_id= $usr_id
            }

        }else{
                $usersReport['exist_mensage'] = 'El correo electrónico no existe en el sistema';
                // $usr_id = '';
            }


    }else if ($result->num_rows>1){
        $usersReport['exist_mensage'] = "La cédula ya está asociada con otro correo electrónico";
    }else{
       $usersReport['exist_mensage'] = "Los datos proporcionados no existen en el sistema";
    }


   return $usersReport;
}

function update_ci($db_handle, $userToMod){
    $email = $userToMod['i_em'];
    $usr_id = $userToMod['i_id'];
    $misionero =  $userToMod['i_mis'];;
    $new_ci =  $userToMod['i_ci'];;

    $sql = $sql = "UPDATE login SET misionero = '$misionero', cedula = '$new_ci' where usr_id= '$usr_id' and email = '$email'";
    // echo $sql;
    $result = $db_handle->query($sql) or ($db_handle->error);
    // var_dump($db_handle);
    // echo $db_handle->affected_rows;
    // var_dump($result);
    return ($db_handle->affected_rows==1)?'<p style="color:green;">Cambio realizado con éxito, la nueva cédula <strong>V-'.$new_ci.'</strong> está ahora asociada con el correo <strong>'. $email.'</strong></p>': (($db_handle->affected_rows==0)? '<p style="color:gray;">No se actualizó nada</p>': '<p style="color:red;">Hubo un error. Contacta a Min :)</p>.');
}


function get_eventos($db_handle){
    $eventosData = array();
    $sql = "SELECT * FROM eventos ORDER BY openRegDate DESC;";
    $result = $db_handle->query($sql) or ($db_handle->error);

    while($row = $result->fetch_assoc())
    {
      $evento['eventoID'] = $row['eventoID'];
      $evento['nombreEvento'] = $row['nombreEvento'];
      $evento['inscripcionUrl'] = $row['inscripcionUrl'];
      $evento['maxCupos'] = $row['maxCupos'];
      $evento['openRegDate'] = $row['openRegDate'];
      $evento['closeRegDate'] = $row['closeRegDate'];
      array_push($eventosData,$evento);
    }
    return $eventosData;
}

function get_inscritos_eventos($db_handle, $eventoID){
    $registrados = array();

    $sql = " SELECT * FROM (SELECT l.usr_id, l.cedula, u.primer_name, u.segun_name, u.primer_last, u.segun_last FROM login as l inner join usuario as u on l.usr_id = u.usr_id) as info inner join eventoPago as a ON info.usr_id = a.misioneroID and a.eventoID = '$eventoID' ORDER BY a.fechaPago;";
    $result = $db_handle->query($sql) or ($db_handle->error);

    // array_push($registrados,array(id,nombre, primer_last,primer_name, cedula, fechaPago, nombreBanco, cedulaTitular, nroReferencia, tipoMoneda, monto));

    while($row = $result->fetch_assoc())
    {
      $misionero['id'] = $row['usr_id'];
      $misionero['CedulaDelMisionero'] = $row['cedula'];
      $misionero['NombreCompleto'] = $row['primer_last'].', '.$row['primer_name'];
      $misionero['PrimerNombre'] = $row['primer_name'];
      $misionero['SegundoNombre'] = $row['segun_name'];
      $misionero['PrimerApellido'] = $row['primer_last'];
      $misionero['SegundoApellido'] = $row['segun_last'];

      $misionero['FechaPago'] = $row['fechaPago'];
      $misionero['NombreDelBanco'] = $row['nombreBanco'];
      $misionero['CedulaTitular'] = $row['cedulaTitular'];
      $misionero['NroReferencia'] = $row['nroReferencia'];
      $misionero['TipoDeMoneda'] = $row['tipoMoneda'];
      $misionero['Monto'] = $row['monto'];
      $misionero['Kilo'] = $row['kilo'];

      array_push($registrados,$misionero);
    }
    return $registrados;
}

function borrar_inscrito_evento($db_handle,$usrToDeregister,$eventoID){

    $id = $usrToDeregister;


    $sql = "SELECT cedula, email FROM login WHERE usr_id = '$id'; ";
    $result = $db_handle->query($sql) or ($db_handle->error);
    if($result->num_rows==0){
        // echo "nothing to do";
        $returnMsg = '<p style="color:gray;">Nada para borrar</p>';
    }else{
        $row = $result->fetch_assoc();
        $ciDel = $row['cedula'];
        $emailDel = $row['email'];
        // this will have to delete the row inserted on the registration table that corresponds to the event and the user.
        $sql = "DELETE FROM eventoPago WHERE misioneroID = $id and eventoID = $eventoID; ";
        // $sql = "UPDATE usuario SET asistir_mision = 0 WHERE usr_id = $id; ";
        $result = $db_handle->query($sql) or ($db_handle->error);
        $returnMsg = '<p style="color:green;">Usuario: <strong>'.$ciDel.'</strong> puede volver a enviar sus datos de pago para formalizar inscripción</p>';
    }
    return $returnMsg;
}



function reporteInscritosAdmin(){
      require "server.php";
      //TODO: this should create a report with all events witin the last SyncReaderWriter
      // make sure to pass in an event ID, so I will have to call the method that gets the events
      // and iterate over that so I can gather info for the report.
      $eventoID = 2; // for now because there is only one
      $m_inscritosViejos = v_getInscritos($mysqli,$eventoID);
      $m_inscritosNuevos = n_getInscritos($mysqli, $eventoID);
      $mysqli->close();

      $reportArray = [["name" => "UnicoEvento (por ahora) M 2020", "nuevos" => $m_inscritosNuevos, "viejos" => $m_inscritosViejos]];
      constructHTMLReporte($reportArray);

}
