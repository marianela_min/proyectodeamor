# Login and registration system of members of Proyecto de Amor for Las Misiones

The no profit catholic organization is in need of a system to store information of current and new members in order to organize Las Misiones.


**What does it contain?**

* Login system that enables members of the organization to register.
    1. Stores user and pwd (using hashes) in a database
    1. Define how to signup a new staff user
    1. Define how the password can be reset
* Design a database that will store relevant characteristics of the memebrs.
    1. Define what the tables will be and the relationship among them
    2. Define primary keys
    3. Define what the columns of each table will be as well as their type and max length.
* Design a portal where the members are able to input their information and register for tbe missions.
    1. Front-end and Back-end of: Login and Signup pages
    2. Front-end and Back-end  of: Login system - forgot password and reset
    3. Front-end of: Show and update personal information.
    4. Back-end of: Insert and update personal information.
    5. Back-end of: Logout script
* Create functions that allow us to:
    1. create(C) elements in a particular table of the database.
    2. read(R) elements in a particular table of the database.
    3. update(U) elements in a particular table of the database. 
    4. delete(D) elements in a particular table of the database.
    5. compute a hash value to store the password.
    6. validation using regex
    7. other usefull functions in case we need to repeat a task multiple times.
