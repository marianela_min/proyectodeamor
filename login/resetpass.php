<?php
require_once "repeat.php";
require_once "../messages/messages.php";

use PHPMailer\PHPMailer\PHPMailer;
//variables that fall through into the html

$failure = 0;
$msg = "";
$email_error ="";
$email = '';

//in the future add cedula so people who forget their email can recover with cedula

if (!isset($_GET['rest'])) {
    redirect(0,"index.php"); exit;
}else{
    if(isset($_POST['submit']) && !empty($_POST['entry_email']))
    {
                
        if ( !empty($_POST['entry_email']) )
        {
            require_once "server.php";
            $email = strtolower($mysqli->escape_string($_POST['entry_email'])); //Escape and set everything to lowercase
            
            if (!preg_match(regexfor('email'),$email)) {
            $email_error = "Ingresa un email válido";
            $failure = $failure +1;
            }

            if ($failure == 0)
            {
                $result = $mysqli->query("SELECT * FROM login WHERE email='$email';");
                if($result->num_rows <= 0){
                    $msg = '<p style="color: green;">Debe registrarse primero.</p>';
                    // echo $msg;
                    // die;
                }
                else{
                    // crear el tocken y enviar correo
                    //    echo 'creating token...';
                    $row = $result->fetch_assoc();
                    if($row['is_confirmed']==0){
                        $title='Requiere activación';
                        $msg = '<p style="color:#ffc107;"><i class="fas fa-exclamation-triangle"></i> Para restaurar su contraseña usted debe confirmar su cuenta primero. Por favor revise la bandeja de entrada de su correo.  No olvide chequear la bandeja de correo no deseado!</p>';
                        include "registroMsg.php";
                        $mysqli->close();
                        die;
                    }
                        
        
                    // echo "This is not changing the DB";
                    $token = 'qwertzuiopasdfghjklyxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM0123456789!$/()*';
                    $token = str_shuffle($token);
                    $token = substr($token, 0, 10);
                    $sql = "UPDATE login SET token='$token', is_reset=0 WHERE email='$email';";
                    $result = $mysqli->query($sql) or ($mysqli->error);
                                                            
                    include_once "../PHPMailer/PHPMailer.php";
                    
                    // echo 'sending email... <br>';
                    // echo $token;
                    $mail = new PHPMailer();
                    $mail->CharSet = 'UTF-8';
                    $mail->setFrom('info@pda.org.ve','Proyecto de Amor');
                    $mail->addAddress($email, "Servidor(a) de PDA");
                    $mail->Subject = "Restauración de Contraseña!";
                    $mail->isHTML(true);
                    $mail->Body =  resetPassMailMsg($email,$token);
                    
                    // if ($mail->send())
                        // $msg = '<p style="color: green; min-height: 30vh;">¡Te hemos enviado un correo de recuperación! Ahora busca en tu bandeja de entrada el correo de activación de tu cuenta, recuerda revisar la carpeta de spam.</p>';
                    // else
                        // $msg = '<p style="color: #f44336; min-height: 30vh;">¡Oops! Algo extraño ocurrió.  No pudimos creat tu cuenta.  Reporta este incidente al administrador del sitio web.  Porfavor Intenta de nuevo.  Gracias por tu paciencia.</p>';
                    $msg = resetPassMsg($mail->send());

                    unset($_POST);
                    $title = "Restauración de Contraseña";
                    include "registroMsg.php";
                    session_unset();
                    session_destroy();
                    die;
                }
            }
            $mysqli->close();
        }
   }
   else{
      $msg = '';
   }
}

?>
<!DOCTYPE html>
<html>
<head>
    <?php include("../css/style_config.php") ?>
    <title>Olvidar Contraseña - Proyecto de amor</title>
</head>
<body>
<main class="content-start">
<div class="container">
<div class="row justify-content-center">
<div class="col-md-6 col-md-offset-3" align="center">
<h1>Proyecto de Amor</h1>
<h4>Restablecer Contraseña</h4>


<img class="img-fluid" width="20%" src="../images/logo.png" alt="PDA logo"><br><br>

<?= $msg ?>

<p>Te enviaremos instrucciones para que puedas restablecer tu contraseña.  Colóca tu correo electrónico en el siguiente recuadro.</p>
<form method="POST" action="resetpass.php?rest=1">
<div>
        <!-- <label for="email">Email: </label> -->
        <input class="form-control" type="text" oninput="isvalid(this.id,'email')" name="entry_email" id="email" placeholder="Email..." value="<?=$email?>" required><span style="color:#f44336;"><?=$email_error?></span>
        <div class="required-message" hidden>Campo requerido</div>
    </div>
    <br/>
    <input type="submit" name="submit" value="Enviar Correo">
</form>
<p>
<a href="login.php">Iniciar Sesión</a>
</p>
</div>


</main>
<?php include("../inc/footer.php") ?>
</body>
</html>