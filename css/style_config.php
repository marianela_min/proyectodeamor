<?php
// require_once("../inc/serverSettings.php");
$confBaseUrl = "https://pda.org.ve";
// meybe play with
// echo $_SERVER['SERVER_NAME'];

?>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">


<!-- Latest compiled and minified CSS -->
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<!-- <link rel="shortcut icon" href="http://pda.org.ve/miembro/favicon.ico" type="image/x-icon"> -->
<link rel="shortcut icon" href="<?=$confBaseUrl?>/miembro/favicon.ico" type="image/x-icon">

<!-- Optional theme -->

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

<!-- <link rel="stylesheet" href="http://pda.org.ve/miembro/css/session.css"> -->
<link rel="stylesheet" href="<?=$confBaseUrl?>/miembro/css/session.css">
<!-- <link rel="stylesheet" href="http://pda.org.ve/miembro/css/fillup.css"> -->
<link rel="stylesheet" href="<?=$confBaseUrl?>/miembro/css/fillup.css">
<!-- <link rel="stylesheet" href="http://pda.org.ve/miembro/css/generalStyle.css"> -->
<link rel="stylesheet" href="<?=$confBaseUrl?>/miembro/css/generalStyle.css">
